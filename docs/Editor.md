# Editor

![EditorImage](./Images/EditorImage.png)

## Rete.js

Rete JS is the main editor/engine framework that was used to develop the node graph. Split into two sections, the *Editor* and the *Engine*; the *Editor* is responsible for handling visually what is happening and user inputs. The *Engine* parses a json view of the nodes using worker functions to generate expected functionality. 

The primary interface between Rete.js code and extended framework is through the binding of events. Rete.js has a list of events [here](https://rete.js.org/#/docs/events). 

### Bound Events

#### process, connection created, connection removed

1. Unselects nodes
2. Updates all nodes
3. Notifies engine to abort process
4. Directs engine to process new layout
5. Updates node service with new layout
6. Updates the critical path
7. Notifies focused nodes of changes

#### node removed

1-6. Identical to above

7. **Removes node from output focus**
8. Notifies focused nodes of changes

#### click

1. Unselects all nodes
2. Updates all nodes

## Node

### Builder

Function defining the structure of the node. Number of inputs and types, number of outputs and types, and controls. All nodes use the [*Item Control*](#item-control) that displays the item as well as various rates for all nodes expect the custom node

### Worker

Function defining the process and transformation of data. Not directly given the individual node instance, instead given the node data such as id, name, etc. Uses a combination of inputs and controls to transform data to place into the output array to pass to connected output nodes. 

#### Connection (Typical worker output structure)

Connection was a datagram that builds up a tree as it gets passed to subsequent nodes. It passes general data about the connection such as nodeId, the item, amount, and per hour rate. The transitionCost and tree represents the complete tree from the current node, down with the transition cost representing the multiple perHour rate from the input to the current output rate. This tree is used primarily in the output node for graphing

```javascript
class Connection {
    public nodeId: number;
    public itemId: number;
    public amount: number;
    public perHour: number;
    public transitionCost: number[];
    public tree: Connection[];
 }
```

#### SocketController

Angular service to create sockets based on item id. Sockets using already created item id are returned the previously created socket. This method would work with the proposed [item subgroup](./Minecraft Factory Planner TDD.md#thoughts- on-wool-(multi-item-treated-as-singular-item))

#### Item Control

The item control is built around two parts. The **Angular Component** and the **Rete Control/Angular Control** portion.

##### Angular Component

Angular component portion of the full rete control. Defines the html/scss. Contains a [Rate Component](#rate-component), [Item Image](./General.md#item-image-component) as well as properties set from the **Rete Control/Angular Control** portion.

##### Rate Control/Angular Control

Uses the **Angular Component** as the visual. Sets the properties of the component, as well as callbacks for retrieving data from the user. Adds data to the `perHour` control member. This is used inside the worker function as the control value.

#### Rate Component

Rate component represents the per hour rate that is given by the user, or by transformations. Has the ability to set the visual rate and has an output value to notify other components of a changed rate.

## Critical Path

The critical path represents the item taking the most time/ or most constrained chain from resource to output node. Every editor process has the potential to change the critical path, and it is calculated based on the editor json. The class `NodeTree` is responsible for decoding and deriving the critical path. 

1. The `NodeTree.parseFromJson` searches through all listed nodes from the editor json that are output nodes
2. For each output node, add to output node list with a new node item
3. `calculateCriticalPath`
   1. Sort output nodes
   2. For each output node, if not `isConnectedToCriticalPath`
      1. Add the node to the critical path
      2. Get the input connection tree. For each connection
         1. `getAllConnectedNodeIds`
            1. Depth first walk gathering all connected node ids
         2. Add connected node ids to list
      3. Filter current critical path list based on if value is included in all connected node list
      4. Return if filtered list size is > 0
   3. Add smallest input connection to stack
   4. While stack has items
      1. Find the `Connection.tree` with the lowest per hour rate. (Greedy search)
      2. Include equal per hour rates
      3. Add all smallest connections to the smallest connection stack
4. Fully generated id path created

Once the path is created, the editor `updatesCriticalPath`. Resets all previous paths, re applies paths to nodes

## Custom Node

Juked up crafting node. Can have **n inputs** and **n outputs**. Critical path is based on the lowest producing input on the lowest producing output. Custom nodes rely on the linear relationship between inputs and outputs based on the crafting transformations they undergo. 

```javascript
enum ShareLevel {
  Personal,
  Group,
  Classroom
}

class CustomNodeMetadata {
  public name: string;
  public shareLevel: ShareLevel;
  public icon: number;
}

class CustomNodeData {
  public uuid: string;
  public uniqueUserId: string;
  public meta: CustomNodeMetadata;
  public inputs!: number[];
  public outputs!: { [outputIndex: number]: { inputRate: { [input: number]: number }, createdAmount: number } };
}
```

For each inputs, and for each outputs, a new socket is created associated with them through the `SocketController`. Extra node data is added called customization. In the worker each outputs critical path is calculated from the linear equation relating input rates to output rates. The connection tree is also generated at this time. Set the *node's* per hour to the smallest found per hour rate for critical path calculation assistance.

## Minecraft Node

Uses real world data collected from Minecraft and retrieved from the backend through the [get inventory](./Backend.md#get-inventory). Updates the rate per hour on a polling system. Currently the application does not poll the backend so this per hour polling never updates the value. Similar in functionality to the *Resource* but the per hour rate cannot be modified.

The item rates itself are updated from callbacks that are registered with the `minecraft-inventory` service. Each time the database is updated, these are called with new averages to update the currently used per-hour rates.

## Sidebar

### ![collapse](./Images/collapse.png) 

Collapse's current graph into custom node. Replaces nodes. Disabled when no input output path found

### ![scale](./Images/scale.png)

Scales the view to contain all the nodes

### ![download](./Images/download.png)

Downloads the current application to a png for submission. Uses the [html2canvas hack](../Readme.md#html2canvas-hack)

### ![expand](./Images/expand.png)

Expands the sidebar

 