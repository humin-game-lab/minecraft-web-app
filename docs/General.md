# General

General items or components used throughout the Minecraft Factory Planner

## Item Model

Items are defined as such

```json
{
      "id": 5,					// Unique id
      "name": "Planks",			// Name
      "index": [4, 0],			// Sprite index for assets/images/inventory_sprites.png
      "craftable": true,		// Is a recipie or resource
      "recipe"?: {				// Not required on non craftable items		
        "createdAmount": 4,		// Number created in a crafting action
        "ingredients": [		// List of ingredients 
          {
            "id": 23,				// Ingredient id
            "amount": 1				// Ingredient count
          }
        ]
      },
      "primaryColor": "#987c4a",	// Graph Color
      "secondaryColor": "#514129"	// Not used
},
```

Items are defined in the `assets/resources/item.json` database. Loaded on application startup, any changes will only be reflected on updating the served version of the application.

## Item Image Component

Universal item image display component. Take an item and boolean if on the critical path as inputs. Item is used to determine the sprite index location for the image. The sprite sheet is used by having a fully transparent image with a moving scaled background that is the sprite sheet.

Used for nodes, node browser, Minecraft items.

[Sprite Coordinate Calculations](https://stackoverflow.com/a/23419418)