# Output Browser

![output browser](./Images/OutputBrowserImage.png)

The **Output Browser** is split into two main portions **Focused** and **Graphs**

## Focused

Focused view displays various nodes that have been highlighted by the user. When a item is highlighted, it is added to a watch list with the `OutputManagerService`. Ever update to the **Editor** propagates to the `OutputServiceManager` that then, depending on the current node updated and the focused items, updates the correct focused item. 

### Children

The individual focused elements have references to a rete.js `node` that, contains data about what is displayed as well as the input and output rates. When the dropdown button is pressed, an event emitter bubbles up the functionality to the output-container that adjusts what nodes are shown. Children are shown based on the current selected(dropped down) node or from the list of all nodes that are currently highlighted.

The progression into and out of children is based on a stack so as the student steps down they are adding nodes to the stack, as they step up they are removing nodes. When the stack is empty, display all focused nodes

#### ![back](./Images/back.png)

Steps up one node in the stack

#### ![top](./Images/top.png)

Steps up all nodes back to primary view

## Graphs

![graph browser](./Images/GraphBrowserImage.png)

**Graph Browser** used *ngx-charts* for the graphing library. [Currently there is a bug related to the legend](../Minecraft Factory Planner TDD.md#graphs).

The graphing behavior is based off output nodes and only outputs will generate graphs in the **Graph Browser**. This is because the primary graphing resource used is converting the current tree state to the [CustomNodeData](./Editor.md#custom-node) for rate calculations.

### Line Graph Behavior

1. Add the current output item to the graph by generating 6 steps at 10 hours each step.
2. Calculate CustomNodeData from the focused output node
3. Calculate highest input rate item.
4. Adjust all input rates by the output rate to find over productions.
5. Accumulate total rates
6. For all input rates greater than zero.
   1. Add to line graph

### Bar Graph Behavior

1. Divide input rate by calculated highest input rate
2. Add 1 - x to bar graph to calculate utilization

### Pie Graph Behavior

1. Add output rate to total rates
2. Divide each valid rate by total rates
3. Place as part of pie graph

