# Minecraft Browser

![minecraft browser](./Images/MinecraftBrowserImage.png)

The **Minecraft Browser** is the direct connection element to the users Minecraft account. It they have linked their account and reached the point where data is logged, this browser will update with the users current inventory. Items are received from the [get inventory](./Backend.md#get-inventory) endpoint. The slots are based on this image

![minecraft inventroy slots](./Images/MinecraftInventorySlots.jpg)

## Grid

The grid is built using HTML `display: grid` with rows being defined with tags. The columns are simply 9 evenly spaced locations. When a item is received, the grid location is calculated based on the slot index from the above graph and a dynamic style is applied to the `inventory-item`. 