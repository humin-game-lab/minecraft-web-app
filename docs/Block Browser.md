# Block Browser

![BlockBrowser](./Images/BlockBrowserImage.png)

Tool box for the students. Any items defined in the local `assets/resources/item.json` will display here. Resource/Craftable filters based on type of item defined. All items can be output nodes.

## Custom Node Filter

Custom nodes are polled once at startup and display on the *All* and *Custom Node* filter. When the *Custom Node* filter is selected after startup. Another refresh API call is sent with the nodes updating when the call returns. Refer to the [get custom nodes](./Backend.md#get-custom-nodes) for communication info.

## Drag And Drop

All nodes are draggable onto the editor. There is two major pieces of information passed from the **Block Browser** to the **Editor**: *NodeType* (Resource, Craftable, Output, Custom) and *NodeId* (or custom nodes uuid). These are passed to the grid `onDrop()` event that then creates the node.

## Dynamic Scaling

Dynamic scaling is done by changing the number of grid columns based on the size of the window.