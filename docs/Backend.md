# Backend

All requests to the backend are post requests. This was used to pass parameters such as sessionId to the backend for filtering without using query params.

## Minecraft Item Backend

### Get Inventory

POST: /minecraft/getInventory

#### Request

````json
{
    "sessionId": "123",			// Session retrieved from the session manager
}
````

#### Response

```json
{
	"current_inventory": [		// Players current inventory
    	{
            "itemId": 1,		// Item at slotIndex
            "slotIndex": 0,		// Slot index in inventory
            "slotCount": 64,	// Slot count in inventory location
        },
        {
            "itemId": 2,
            "slotIndex": 1,
            "slotCount": 32,
        },
        ...
    ],
    "averages": {				// Averages of items above. All items above has element in averages
        "1": 1.0,
        "2": 0.5,
     }
}
```

#### Notes

Current implementations due to cognitive walkthrough* requirements add 

```json
{
	"itemId": 23,		// Logs
	"slotCount": 23,
	"slotIndex": 6		
}
```

to everyones inventory, **except** test account #123

## Custom Nodes

### Get Custom Nodes

POST /minecraft/getNodes

#### Request

```json
{
    "sessionId": "123",			// Session retrieved from the session manager
}
```

#### Response

```json
{
    "count": 2
	"Items": [
		<CustomNodeData>,
		<CustomNodeData>,
	]
}
```

[CustomNodeData](./Editor.md#custom-node)

### Save Custom Nodes

POST /minecraft/postNode

#### Request

```json
{
	"sessionId": "123",
	"nodeData": <CustomNodeData>
}
```

#### Response

```json
{
    "statusCode": 
	"message": {
		"uuid": "uuid-12345-abcdefg"
	}
}
```

