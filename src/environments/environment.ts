// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

export const environment_vars = {
  // api_root: "http://127.0.0.1:3000",
  // api_root: "https://vz991lfr58.execute-api.us-east-1.amazonaws.com/Prod",
  api_root: "https://zk2qx4ml4b.execute-api.us-east-1.amazonaws.com/Prod",
  authentication: 'SmQ0aUNXdWh3ZgpNOFhLTnZpQlYwCmxzZzZWRE1tQVMKSWdwUnRjcEhvWgpubU1Qb3hEMWhP',

  // TODO: Make this set from URL params
  sessionId: 'user1-id'
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
