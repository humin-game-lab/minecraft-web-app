import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InventoryModule } from './inventory/inventory.module';
import { OutputModule } from './output/output.module';
import { SocketController } from './rete-modules/nodes/sockets';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { CustomReteModule } from './rete-modules/custom-rete.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CustomReteModule,
    OutputModule,
    InventoryModule,
    NgbModule,
  ],
  providers: [SocketController],
  bootstrap: [AppComponent],
})
export class AppModule {}

