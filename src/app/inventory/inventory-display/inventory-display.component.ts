import { ItemImageComponent } from './../../general/item-image-component/item-image.component';
import { Item, UNKNOWN_ITEM } from './../../general/model/item.model';
import { ItemService } from 'src/app/rete-modules/services/item.service';
import { MinecraftItem } from './../shared/item/minecraft-item.model';
import { Component, OnInit, ViewChildren } from '@angular/core';
import { MinecraftInventoryService } from '../services/minecraft-inventory.service';

@Component({
  selector: 'app-inventory-display',
  templateUrl: './inventory-display.component.html',
  styleUrls: ['./inventory-display.component.sass']
})
export class InventoryDisplayComponent {
  public minecraftItems: MinecraftItem[];

  @ViewChildren(ItemImageComponent) minecraftItemViews: ItemImageComponent;

  constructor(private minecraftInventory: MinecraftInventoryService, private itemService: ItemService) { 
    this.minecraftItems = [];
    this.minecraftInventory.ForceGetAllInventoryItems().then(value => {
      this.minecraftItems = value;
      console.log(this.minecraftItems);
    });
  }

  // Note: Minecraft items are base 0, javascript grids are base 1 for starting index
  //  that is the reason +1 is added to everything
  GetGridStyle(item: MinecraftItem): any {
    // Item is in the hotbar
    let gridLocation = {};
    if (item.slotIndex <= 8) {
      gridLocation['grid-row'] = 'hotbar';
      gridLocation['grid-column'] = item.slotIndex + 1;
      return gridLocation;
    }

    // Inventory slots start at 9 after the hot bar. Adjusting to 0-27
    let inventorySlot = item.slotIndex - 9;
    gridLocation['grid-row'] = Math.floor(inventorySlot / 9) + 1;
    gridLocation['grid-column'] = (inventorySlot % 9) + 1;

    return gridLocation;
  }

  Refresh() {
    this.minecraftInventory.ForceGetAllInventoryItems().then(value => {
      this.minecraftItems = value;
    });
  }
}
