import { Item } from './../../../general/model/item.model';

export class MinecraftItem {
  public slotIndex: number;
  public itemId: number;
  public slotCount: number;
  public metadata?: any;
}