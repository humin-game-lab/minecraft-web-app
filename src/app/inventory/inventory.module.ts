import { GeneralModule } from './../general/general.module';
import { HttpMinecraftInventoryService } from './services/http-minecraft-inventory.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinecraftInventoryService } from './services/minecraft-inventory.service';
import { InventoryDisplayComponent } from './inventory-display/inventory-display.component';
import { InventoryItemComponent } from './inventory-item/inventory-item.component';



@NgModule({
  declarations: [InventoryDisplayComponent, InventoryItemComponent],
  imports: [
    CommonModule,
    GeneralModule
  ],
  providers: [{ provide: MinecraftInventoryService, useClass: HttpMinecraftInventoryService }],
  exports: [ InventoryDisplayComponent ],
})
export class InventoryModule { }
