import { MinecraftItem } from './../shared/item/minecraft-item.model';
import { Injectable } from '@angular/core';

export let MILLISECONDS_TO_HOURS = 3600000

class ItemCallback {
  thisRef: any;
  nodeId: number;
  callback: (nodeId: number, avg: number) => void;

  constructor(thisRef: any, nodeId: number, callback: (nodeId: number, avg: number) => void) {
    this.thisRef = thisRef;
    this.nodeId = nodeId;
    this.callback = callback;
  }
}

type InvetoryCallbacks = { [ itemId: number ]: ItemCallback[] }

@Injectable({
  providedIn: 'root'
})
export abstract class MinecraftInventoryService {
  private inventoryCallbacks: InvetoryCallbacks;

  constructor() { 
    this.inventoryCallbacks = {};
  }

  public abstract GetAllInventoryItems(): Promise<MinecraftItem[]>;
  public abstract ForceGetAllInventoryItems(): Promise<MinecraftItem[]>;
  public abstract GetInventoryItemAverage(id: number): Promise<number>;

  public InventoryUpdateCallbacks() {
    for (let itemIdLabel in this.inventoryCallbacks) {
      let itemId = parseInt(itemIdLabel);
      this.GetInventoryItemAverage(itemId).then(average => {
        this.inventoryCallbacks[itemIdLabel].forEach(callback => {
          // Call the callback passing this ref to be used
          callback.callback.call(callback.thisRef, callback.nodeId, average);
        });
      });
    }
  }

  public AssignInventoryUpdateCallback(thisRef: any, nodeId: number, itemId: number, callback: (nodeId: number, avg: number) => void) {
    let item: ItemCallback = { thisRef, nodeId, callback };

    // Add callback to item id list
    if (this.inventoryCallbacks[itemId]) {
      this.inventoryCallbacks[itemId].push(item)
    } else {
      this.inventoryCallbacks[itemId] = [item];
    }
  }
}
