import { TestBed } from '@angular/core/testing';

import { MinecraftInventoryService } from './minecraft-inventory.service';

describe('MinecraftInventoryService', () => {
  let service: MinecraftInventoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MinecraftInventoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
