import { MinecraftItem } from './../shared/item/minecraft-item.model';
import { Injectable } from '@angular/core';
import { MinecraftInventoryService, MILLISECONDS_TO_HOURS } from './minecraft-inventory.service';

let fake_db = {
  "current_inventory": [
      {
          "itemId": 2,
          "slotIndex": 14,
          "slotCount": 3
      },
      {
          "itemId": 1,
          "slotIndex": 3,
          "slotCount": 37
      },
      {
          "itemId": 5,
          "slotIndex": 5,
          "slotCount": 3
      }
  ],
  "averages": {
      "1": 0.0035,
      "2": -0.0045,
      "6": 0
  }
}

@Injectable({
  providedIn: 'root'
})
export class FakeMinecraftInventoryService extends MinecraftInventoryService {
  private mostRecentTimeMs = 1613018235147;
  private itemOneQuantity = 84;
  private itemTwoQuantity = 40;
    
  constructor() { 
    super();
  }

  public GetAllInventoryItems(): Promise<MinecraftItem[]> {
    return Promise.resolve(fake_db.current_inventory);
  }

  public ForceGetAllInventoryItems(): Promise<MinecraftItem[]> {
    return Promise.resolve(fake_db.current_inventory);
  }

  public GetInventoryItemAverage(id: number): Promise<number> {
    let averageOverHour = 0;
    if (fake_db.averages[id]) {
      averageOverHour = fake_db.averages[id];
    }

    return Promise.resolve(averageOverHour * MILLISECONDS_TO_HOURS);
  }
}
