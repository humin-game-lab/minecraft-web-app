import { TestBed } from '@angular/core/testing';

import { FakeMinecraftInventoryService } from './fake-minecraft-inventory.service';

describe('FakeMinecraftInventoryService', () => {
  let service: FakeMinecraftInventoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FakeMinecraftInventoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
