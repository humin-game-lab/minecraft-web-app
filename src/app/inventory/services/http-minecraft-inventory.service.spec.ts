import { TestBed } from '@angular/core/testing';

import { HttpMinecraftInventoryService } from './http-minecraft-inventory.service';

describe('HttpsMinecraftInventoryService', () => {
  let service: HttpMinecraftInventoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpMinecraftInventoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
