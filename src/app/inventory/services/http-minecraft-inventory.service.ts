import { SessionService } from 'src/app/general/service/session.service';
import { environment_vars } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { MinecraftItem } from '../shared/item/minecraft-item.model';
import { Injectable } from '@angular/core';
import { MILLISECONDS_TO_HOURS, MinecraftInventoryService } from './minecraft-inventory.service';

let getMinecraftItemRoute = "/minecraft/getInventory"

const options = {
  headers: {
    Authorization: environment_vars.authentication
  }
}

@Injectable({
  providedIn: 'root'
})
export class HttpMinecraftInventoryService extends MinecraftInventoryService {
  private localDataBaseStorage: any = {};

  constructor(private httpClient: HttpClient, private sessionService: SessionService ) { 
    super();

    this.localDataBaseStorage = {};
  }
  
  public GetAllInventoryItems(): Promise<MinecraftItem[]> {
    return this.UpdateLocalStorageIfNeeded(-1, false);
  }

  public ForceGetAllInventoryItems(): Promise<MinecraftItem[]> {
    return this.UpdateLocalStorageIfNeeded(-1, true);
  }

  public GetInventoryItemAverage(id: number): Promise<number> {
    return this.UpdateLocalStorageIfNeeded(id, false);
  }

  private GetFromLocalStorage(id: number): Promise<any> {
    if (id === -1) {
      return Promise.resolve(this.localDataBaseStorage.current_inventory);
    }

    if (this.localDataBaseStorage.averages && this.localDataBaseStorage.averages[id]) {
      return Promise.resolve(this.localDataBaseStorage.averages[id] * MILLISECONDS_TO_HOURS);
    }

    // TODO: Temorary addition to provide rates to all inventory items
    //    return Promise.resolve(0);
    //return Promise.resolve(1 + Math.round(Math.random()*25));
    return Promise.resolve(20);
  }
  
  private UpdateLocalStorageIfNeeded(id: number, forceUpdate: boolean): Promise<any> {
    let body = this.sessionService.buildPostWithSession("", null);

    if (forceUpdate || this.localDataBaseStorage === {}) {
      return (this.httpClient.post(environment_vars.api_root + getMinecraftItemRoute, body, options).toPromise() as Promise<any>).then(any => {
        this.localDataBaseStorage = any;

        // TODO: Temorary addition for congnitive walkthrough
        if (!this.localDataBaseStorage.current_inventory) {
          this.localDataBaseStorage = {};
          this.localDataBaseStorage.current_inventory = []
          this.localDataBaseStorage.averages = {};
        }

        if (this.sessionService.getSession() !== "123") {
          this.localDataBaseStorage.current_inventory.push(
            {
              "itemId": 23,
              "slotCount": 23,
              "slotIndex": 6
            }
          );
          this.localDataBaseStorage.averages['23'] = 0.000085
        }
          
        this.InventoryUpdateCallbacks();
        return this.GetFromLocalStorage(id);
      });
    }

    return this.GetFromLocalStorage(id);
  }
}