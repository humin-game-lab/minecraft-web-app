import { ItemImageComponent } from './../../general/item-image-component/item-image.component';
import { Item, UNKNOWN_ITEM } from './../../general/model/item.model';
import { ItemService } from './../../rete-modules/services/item.service';
import { MinecraftItem } from './../shared/item/minecraft-item.model';
import { Component, OnInit, ViewChildren, Input } from '@angular/core';

@Component({
  selector: 'app-inventory-item',
  templateUrl: './inventory-item.component.html',
  styleUrls: ['./inventory-item.component.sass']
})
export class InventoryItemComponent implements OnInit {
  @Input() minecraftItem: MinecraftItem;
  public item: Item;
  public isValid: boolean = true;

  @ViewChildren(ItemImageComponent) minecraftItemViews: ItemImageComponent;

  constructor(private itemService: ItemService) {
  }

  ngOnInit(): void {
    if (this.minecraftItem.itemId == UNKNOWN_ITEM.id) {
      this.item = UNKNOWN_ITEM;
      this.isValid = false;
    } else {
      this.item = this.itemService.GetItemFromId(this.minecraftItem.itemId);
    }
  }

  ngAfterViewInit() {
    this.minecraftItemViews.selectedItem = this.item;
  }

  OnDrag(event: any) {
    event.dataTransfer.setData("nodeType", "Minecraft")

    event.dataTransfer.setData("nodeId", this.item.id);
    event.dataTransfer.effectAllowed = "copy";
  }
}
