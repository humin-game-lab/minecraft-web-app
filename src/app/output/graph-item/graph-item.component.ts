/** Current issue with ngx-charts 
 *    Legend height is not calculated in auto height when using legend="below"
 *    Results in incorrect styling when not defining a view=[x,y] for responsive design
 * 
 *    Current open pull request #1520 on ngx-charts github. Oct 1, 2020
 * 
 *    https://github.com/swimlane/ngx-charts/pull/1520
 */

import { ItemService } from 'src/app/rete-modules/services/item.service'
import { NodeService } from 'src/app/rete-modules/services/node.service';
import { OutputManagerService } from './../services/output-manager.service';
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Node } from 'rete';
import { Color } from 'src/app/general/model/color.model';

class LineChartData
{
  public name: string = "data";
  public series: { name: string, value: number }[];

  constructor(name: string, perHourRate: number, steps: number, stepSize: number) {
    this.name = name;

    this.series = [];
    for (let step = 0; step < steps; step++){
      if (perHourRate == 0 || perHourRate == Infinity) {
        continue;
      }
      this.series.push( {"name":String(step*stepSize), "value":step*stepSize*perHourRate })
    }
  }
}


const plannedWidth = 1920;


@Component({
  selector: 'app-graph-item',
  templateUrl: './graph-item.component.html',
  styleUrls: ['./graph-item.component.sass']
})
export class GraphItemComponent implements OnInit {
  @Input() node: Node; 

  // Line Chart Data
  public lineView: number[] = [400, 250];
  public timeline: boolean = true;
  public primaryData: any[] = [];

  // Bar Chart Data
  public secondaryData: any[] = [];
  public barView: number[]  = [100, 100];
  
  // Pie Chart Data
  public tertiaryData: any[] = [];
  public pieView: number[] = [100, 100]
  public showPieLabels: boolean = false;

  // Universal data
  public gradient: boolean = true;
  public itemName: string = "Loading";
  public id: number = 0;

  // Temporary
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Year';
  yAxisLabel: string = 'Population';

  colorSchemeLine = {
    domain: []
  };

  colorSchemeBar = {
    domain: []
  };

  colorSchemePie = {
    domain: []
  };

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    let ratio = (event.target.innerWidth / plannedWidth)
    this.barView[0] = (this.lineView[0] / 2.2) * ratio;
    this.pieView[0] = (this.lineView[0] / 2.2) * ratio;
  }

  constructor(private outputManagerService: OutputManagerService, private nodeService: NodeService, private itemService: ItemService) {
    
   }

  ngOnInit(): void {
    let ratio = window.innerWidth / plannedWidth;
    this.barView[0] = (this.lineView[0] / 2.2) * ratio;
    this.pieView[0] = (this.lineView[0] / 2.2) * ratio;
    this.outputManagerService.bindOnNodeUpdate(this.node.id, val => {
      this.node = val;

      this.updateData();

      this.itemName = this.node.data['resource']['name'];
      this.id = this.node.id;
    })

    this.updateData();
  }

 
  private updateData() {
    this.primaryData = [];
    this.secondaryData = [];
    this.tertiaryData = [];

    this.colorSchemeLine.domain = [];
    this.colorSchemeBar.domain = [];
    this.colorSchemePie.domain = [];

    let nodeData = this.node.data;
    let nodePerHour = <number>nodeData['perHour'];
    let outputData = new LineChartData(nodeData['resource']['name'], nodePerHour, 6, 10);
    this.primaryData.push(outputData)
    this.colorSchemeLine.domain.push(this.itemService.GetItemPrimaryColor(nodeData['resource']['id']).toHex());

    let localNode = this.nodeService.getNodeTreeItem(this.node.id);
    let result = this.nodeService.calculateCustomNodeFromOutputs( localNode ? [localNode] : [] );
    let nodeIds = result[0];
    let calcuatedInputData = result[1];

    let highestRate = 0;
    let totalRates = 0;
    let inputRates: { [id: number]: number } = {};

    //TODO: This does not work for custom nodes. Need to separate graphs based on outputs
    for (let outputId in calcuatedInputData.outputs) {
      let output = calcuatedInputData.outputs[outputId];

      calcuatedInputData.inputs.forEach(inputId => {
        let inputRate = calcuatedInputData.outputs[outputId].createdAmount * calcuatedInputData.outputs[outputId].inputRate[inputId];
        nodeIds.forEach(value => {
          let node = this.outputManagerService.getNodeById(value);
          if (node.data['resource']['id'] == inputId) {
            inputRate *= <number>node.data['perHour'];
          }
        })

        if (inputRate > highestRate) {
          highestRate = inputRate;
        }
  
        inputRate -= nodePerHour;
        totalRates += inputRate;
  
        inputRates[inputId] = inputRate;
      })
    }

    totalRates += nodePerHour;

    for (let inputId in inputRates) {
      let inputRate = inputRates[inputId];
      let inputItem = this.itemService.GetItemFromId(parseInt(inputId));

      // If we are generating excess resources. Add to the primaryData table
      if (inputRate > 0) {
        let outputData = new LineChartData(inputItem.name, inputRate, 6, 10);
        this.primaryData.push(outputData);
        this.colorSchemeLine.domain.push(this.itemService.GetItemPrimaryColor(inputItem.id).toHex());
      }

      let utilization = inputRate / highestRate;

      if (Math.abs(utilization - 1) > .00001) {
        this.secondaryData.push({ "name": inputItem.name, "value": (1 - utilization) * 100 });
        let color = this.itemService.GetItemPrimaryColor(inputItem.id);

        this.AddToLegend(inputItem.name, color);
        this.colorSchemeBar.domain.push(this.itemService.GetItemPrimaryColor(inputItem.id).toHex());
      }
      
      if (Math.abs(inputRate - 0) > .00001) {
        this.tertiaryData.push({ "name": inputItem.name, "value": inputRate / totalRates });
        let color = this.itemService.GetItemPrimaryColor(inputItem.id);

        this.AddToLegend(inputItem.name, color);
        this.colorSchemePie.domain.push(this.itemService.GetItemPrimaryColor(inputItem.id).toHex());
      }
    }
    
    // Add the output item to the output ratios
    if (nodePerHour != Infinity) {
      this.tertiaryData.push({ "name": nodeData['resource']['name'], "value": nodePerHour / totalRates });
      let color = this.itemService.GetItemPrimaryColor(nodeData['resource']['id']);

      this.AddToLegend(nodeData['resource']['name'], color);
      this.colorSchemePie.domain.push(color.toHex());
    }
  }


  private AddToLegend( name: string, color: Color) {
    let found = this.primaryData.find(value => {
      return value['name'] == name
    })

    if (found) { return; }
    
    let legendData = new LineChartData(name, 0, 0, 0);
    this.primaryData.push(legendData);
    this.colorSchemeLine.domain.push(color.toHex());
  }
}
