import { Connection } from './../../rete-modules/shared/connection.model';
import { OutputManagerService } from './../services/output-manager.service';
import { Component, OnInit } from '@angular/core';
import { NodeService } from 'src/app/rete-modules/services/node.service';
import { Node } from 'rete';

@Component({
  selector: 'app-output-container',
  templateUrl: './output-container.component.html',
  styleUrls: ['./output-container.component.sass']
})

export class OutputContainerComponent implements OnInit {
  public markedNodes: Node[];
  public nodeStack: Node[];
  
  public hasSelectedNode: boolean = false;
  public selectedNode: Node;

  // Handle the tabs at the top
  public isFocusSelected: boolean = true;
  onFocusClicked() {
    this.isFocusSelected = true;
  }
  onGraphClicked() {
    this.isFocusSelected = false;
  }
  
  constructor(private outputManager: OutputManagerService, private nodeService: NodeService) {
    this.nodeStack = [];

    setInterval(() => {
      this.markedNodes = this.outputManager.getMarkedNodes();
    }, 500)
  }

  ngOnInit(): void {
  }

  dropdownCallback(node: Node) {
    if( this.selectedNode ) {
      this.nodeStack.push(this.selectedNode);
    }

    this.selectedNode = node;
    this.hasSelectedNode = true;
  }

  popNode() {
    if (this.nodeStack.length > 0) {
      this.selectedNode = this.nodeStack.pop();
    } else {
      this.topNode();
    }
  }

  topNode() {
    this.selectedNode = null;
    this.hasSelectedNode = false;
    this.nodeStack = [];
  }

  getSelectedChildren(): Node[] {
    let children = [];
    if (this.selectedNode) {
      let tree = <Connection[]>this.selectedNode.data['inputTree'];
      if (tree) {
        for (let id in tree) {
          let treeValue = tree[id];

          children.push(this.outputManager.getNodeById(treeValue.nodeId));
        }
      }
    }


    return children;
  }

  getCriticalPath() {
    return this.nodeService.getCriticalPath()
  }

  getCriticalPathObjects() {
    let criticalPathNodes = []
    for(let id in this.nodeService.getCriticalPath()) {
      let nodeObject = JSON.stringify(this.nodeService.getNodeTreeItem(parseInt(id)))

      // console.log("object " + nodeObjectJSON.data)
      // if(nodeObject != null)
      //   criticalPathNodes.push(nodeObject.getNodeName())
      // else
      //   criticalPathNodes.push("name not found")
      criticalPathNodes.push(nodeObject)
    }
    return criticalPathNodes
  }
}

// null, { 
//   "node": { 
//     "id": 1, 
//     "data": { "resource": { "id": 5, "name": "Planks", "index": [4, 0], "craftable": true, "recipe": { "createdAmount": 4, "ingredients": [{ "id": 23, "amount": 1 }] }, "primaryColor": { "r": 152, "g": 124, "b": 74 }, "secondaryColor": { "r": 81, "g": 65, "b": 41 } }, "name": "Output", "amount": 1, "perHour": null, "inputTree": [{ "nodeId": 2, "itemId": 5, "amount": 4, "perHour": null, "transitionCost": [1], "tree": [{ "nodeId": 3, "itemId": 23, "amount": 1, "transitionCost": [], "tree": [] }] }] },
//     "inputs": { "resource": { "connections": [{ "node": 2, "output": "resource", "data": {} }] } }, "outputs": {}, "position": [1060, 183], "name": "Output" }, "inputs": [], "outputs": [] }, null