import { Node, NodeEditor } from 'rete';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OutputManagerService {
  private editor: NodeEditor;
  private markedNodes: Node[];
  private onNodeUpdate: { [nodeId: number]: Function[] };

  constructor() { 
    this.markedNodes = [];
    this.onNodeUpdate = {};
  }

  bindOnNodeUpdate(nodeId: number, func: Function) {
    if (this.onNodeUpdate[nodeId]) {
      this.onNodeUpdate[nodeId].push(func);
    } else {
      this.onNodeUpdate[nodeId] = [func];
    }
  }

  setEditor( editor: NodeEditor ) {
    this.editor = editor;
  }

  update( nodes: Node[] ) {
    nodes.forEach(node => {
      if (this.onNodeUpdate[node.id]) {
        this.onNodeUpdate[node.id].forEach(callback => {
          callback(node);
        })
      }
    })
  }

  getNodeById(nodeId: number): Node {
    return this.editor.nodes.find(value => value.id == nodeId);
  }

  getMarkedNodes(): Node[] {
    return this.markedNodes;
  }

  getNumberMarkedNodes(): number {
    return Object.keys(this.markedNodes).length;
  }

  markNode(nodeId: number) {
    let node = this.getNodeById(nodeId);
    this.markedNodes.push(node);
  }
  
  unmarkNode(nodeId: number) {
    let tempNodes: Node[] = [];
    
    this.markedNodes.forEach(value => {
      if (value.id != nodeId) {
        tempNodes.push(value);
      }
    })

    this.markedNodes = tempNodes;
  }
}
