import { Item } from './../../general/model/item.model';
import { CustomNodeData } from './../../rete-modules/shared/custom.model';
import { ItemService } from 'src/app/rete-modules/services/item.service';
import { Connection } from './../../rete-modules/shared/connection.model';
import { OutputManagerService } from './../services/output-manager.service';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { Node } from 'rete';

class RelativeNodeData {
  public id: number;
  public name: string;
  public rate: number;

  constructor(id: number, name: string, rate: number) {
    this.id = id;
    this.name = name;
    this.rate = rate;
  }
}

@Component({
  selector: 'app-output-item',
  templateUrl: './output-item.component.html',
  styleUrls: ['./output-item.component.sass']
})
export class OutputItemComponent implements OnInit {
  @Input() node: Node;
  @Output() dropdownPressed = new EventEmitter<Node>();

  public inputs: RelativeNodeData[] = [];
  public outputs: RelativeNodeData[] = [];

  constructor(private outputManagerService: OutputManagerService, private itemService: ItemService) {
  }
  
  ngOnInit(): void {
    this.outputManagerService.bindOnNodeUpdate(this.node.id, val => {
      this.node = val;
      console.log("Node Updated");

      this.updateInputs();
      this.updateOutputs();
    })

    this.updateInputs();
      this.updateOutputs();
  }

  getName(): string {
    return <string>this.node.data['name'];
  }

  getId(): number {
    return this.node.id;
  }

  showDropdown(): boolean {
    let shouldShow = false;

    if (this.node.data['inputTree']) {
      if ((<any[]>this.node.data['inputTree']).length > 0) {
        shouldShow = true;
      }
    }

    return shouldShow;
  }

  toggleDropdown() {
    this.dropdownPressed.emit(this.node)
  }

  getType(): string {
    return <string>this.node.meta['type'];
  }

  isCriticalPath(): string {
    return this.node.meta.criticalPath ? 'critical-path' : "";
  }

  private updateInputs() {
    this.inputs = [];

    if (this.node.meta['type'] == 'output') {
      return;
    }

    let nodeTree = <Connection[]>this.node.data['inputTree'];
    if (nodeTree) {
      nodeTree.forEach(nodeInput => {
        this.inputs.push( new RelativeNodeData( nodeInput.itemId, this.itemService.GetItemFromId(nodeInput.itemId).name, nodeInput.amount * nodeInput.perHour ) );
      })
    }
  }

  private updateOutputs() {
    this.outputs = [];
    
    if (this.node.meta['type'] == "custom") {
      let customData: CustomNodeData = <CustomNodeData>this.node.data['customization'];
      for (let outputIndex in customData.outputs) {
        let outputData = customData.outputs[outputIndex];
        let smallestRate = Infinity;

        for (let inputIndex in outputData.inputRate) {
          let inputRate = outputData.inputRate[inputIndex];
          let relativeInputData = this.inputs.find(value => {
            return value.id == parseInt(inputIndex);
          });

          if (relativeInputData) {
            if (inputRate * relativeInputData.rate < smallestRate) {
              smallestRate = inputRate * relativeInputData.rate;
            }
          }
        }

        let outputId = parseInt(outputIndex);
        this.outputs.push(new RelativeNodeData(outputId, this.itemService.GetItemFromId(outputId).name, smallestRate));
      }
    } else {
      let item: Item = <Item>this.node.data['resource'];
      this.outputs.push(new RelativeNodeData(item.id, item.name, Number(this.node.data['perHour'])));
    }
  }
}
