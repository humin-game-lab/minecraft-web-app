import { AppModule } from './../app.module';
import { GeneralModule } from './../general/general.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutputContainerComponent } from './output-container/output-container.component';
import { OutputManagerService } from './services/output-manager.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { OutputItemComponent } from './output-item/output-item.component';
import { GraphViewComponent } from './graph-view/graph-view.component';
import { GraphItemComponent } from './graph-item/graph-item.component';


@NgModule({
  declarations: [OutputContainerComponent, OutputItemComponent, GraphViewComponent, GraphItemComponent],
  imports: [
    CommonModule,
    NgxChartsModule,
    FormsModule,
    GeneralModule,
  ],
  exports: [OutputContainerComponent,
  ],
  providers: [OutputManagerService]
})
export class OutputModule { }
