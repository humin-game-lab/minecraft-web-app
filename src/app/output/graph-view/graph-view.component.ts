import { OutputManagerService } from './../services/output-manager.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graph-view',
  templateUrl: './graph-view.component.html',
  styleUrls: ['./graph-view.component.sass']
})
export class GraphViewComponent implements OnInit {
  public markedNodes: any[] = [];

  constructor(private outputManager: OutputManagerService) {

    setInterval(() => {
      let nodes = this.outputManager.getMarkedNodes();
      this.markedNodes = nodes.filter(value => {
        return value.meta['type'] == "output";
      })
    }, 500)
  }

  ngOnInit(): void {
  }

}
