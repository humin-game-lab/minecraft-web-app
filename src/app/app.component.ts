import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from './general/service/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  enteredId: boolean = false;
  idString: string = "";

  constructor(private route: ActivatedRoute, private sessionService: SessionService) {
    this.route.queryParams.subscribe(params => {
      if (params.id)
      {
        this.sessionService.setSession(params.id);
        this.enteredId = true;
      }
    })
  }

  submitDisabled() {
    return this.idString === '';
  }

  submit() {
    this.sessionService.setSession(this.idString);
    this.enteredId = true;
  }
}
