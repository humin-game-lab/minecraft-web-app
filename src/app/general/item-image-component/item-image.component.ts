import { Item } from './../model/item.model';
import { Component, Input, Output } from "@angular/core";

let spriteSize = 32;
let spriteScale = 2.2;

// Item component that displays and updates the item visible in the image.
// Takes a Item as an input and will update the visual when updateVisual is called
@Component({
  selector: 'item-image',
  templateUrl: 'item-image.component.html',
  styleUrls: ['item-image.component.sass']
})
export class ItemImageComponent {
  @Input() selectedItem: Item;
  @Input() criticalPath: boolean;

  public background: any;

  constructor() {
    this.background = {};
  }

  ngOnInit() {
    this.updateVisual( this.selectedItem);
  }

  updateVisual(item: Item) {
    // Reactive sprite coordinate calculation https://stackoverflow.com/a/23419418
    let spritePixelCoords = { x: item.index[0] * spriteSize, y: item.index[1] * spriteSize };

    this.background['background-position-x'] = spritePixelCoords.x / (512 - spriteSize) * 100 + "%";
    this.background['background-position-y'] = spritePixelCoords.y / (1120 - spriteSize) * 100 + "%";
  }
}