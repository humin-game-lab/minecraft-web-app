import { HighlightSearchPipe } from './pipe/highlight-text.pipe';
import { ItemImageComponent } from './item-image-component/item-image.component';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SessionService } from './service/session.service'
import { AppRoutingModule } from '../app-routing.module'



@NgModule({
  declarations: [
    IconButtonComponent,
    ItemImageComponent,
    HighlightSearchPipe,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
  ],
  providers: [ SessionService ],
  exports: [IconButtonComponent, ItemImageComponent, HighlightSearchPipe ]
})
export class GeneralModule { }
