class RecipeComponent {
  public id: number;
  public amount: number;

  constructor(id: number, amount: number) {
    this.id = id;
    this.amount = amount;
  }
}

export class Recipe {
  public createdAmount: number;
  public ingredients: RecipeComponent[];

  constructor(createdAmount: number, ingredients: RecipeComponent[]) {
    this.createdAmount = createdAmount;
    this.ingredients = ingredients;
  }
}