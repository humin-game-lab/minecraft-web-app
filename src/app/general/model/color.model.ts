export class Color {
  public r: number = 255;
  public g: number = 255;
  public b: number = 255;

  static FromHex( hex: String): Color {
    let r = hex.substr(1, 2);
    let g = hex.substr(3, 2);
    let b = hex.substr(5, 2);

    return new Color(parseInt(r, 16), parseInt(g, 16), parseInt(b, 16));
  }

  constructor(r: number, g: number, b: number) {
    this.r = r;
    this.g = g;
    this.b = b;
  }

  toHex(): string {
    return "#" + this.r.toString(16).padStart(2, '0') + this.g.toString(16).padStart(2, '0') + this.b.toString(16).padStart(2, '0') ;
  }
}