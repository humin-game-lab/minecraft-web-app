import { Color } from './color.model';
import { Recipe } from './recipe.model';

export const UNKNOWN_ITEM = {
  id: -1,
  name: "Unknown",
  index: [14, 0],
  craftable: false,

  primaryColor: new Color(255, 0, 255),
  secondaryColor: new Color(0, 255, 255)
}

export class Item {
  public id!: number;
  public name!: string;
  public index: any;
  public craftable: boolean;
  public createdAmount?: number;
  public recipe?: Recipe;

  public primaryColor: Color;
  public secondaryColor: Color;

  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}