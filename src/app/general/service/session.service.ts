import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private sessionId: string = "NO_SESSION";
  private hasBeenSet = false;

  constructor() {
  }

  setSession( newSession ) {
    if (this.hasBeenSet) {
      console.error("WARNING: Session is already set");
    }

    this.sessionId = newSession;
  }

  getSession(): string {
    if (!this.hasBeenSet) {
      console.error("WARNING: Session has not be set");
    }

    return this.sessionId;
  }

  buildPostWithSession(bodyTag: string, body: any) {
    let postBody: any = {};
    postBody.sessionId = this.sessionId;

    if (bodyTag !== "") {
      postBody[bodyTag] = body;
    }

    return postBody;
  }
}
