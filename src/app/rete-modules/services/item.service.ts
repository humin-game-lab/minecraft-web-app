import { Color } from './../../general/model/color.model';
import { Item } from  '../../general/model/item.model';
import { Injectable } from '@angular/core';

import * as jsonFileData from '../../../assets/resources/items.json';

let loadedData = jsonFileData['default'];

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private localDatabase = {};
  constructor() {
    this.localDatabase = loadedData;
    this.localDatabase['items'].forEach(item => {
      if (item.primaryColor) {
        item.primaryColor = Color.FromHex(item.primaryColor)
      } else {
        item.primaryColor = new Color(1, 1, 1);
      }
      if (item.secondaryColor) {
        item.secondaryColor = Color.FromHex(item.secondaryColor)
      } else {
        item.secondaryColor = new Color(1, 1, 1);
      }
    });
  }
  
  public GetAllItems(): Item[] {
    return this.localDatabase['items'];
  }

  public GetItemFromId(id: number): Item {
    return this.localDatabase['items'].find((item) => {
      return item.id == id;
    })
  }
  public GetAllCraftableItems(): Item[] {
    return this.localDatabase['items'].filter((item) => {
      return item.craftable;
    })

  }
  public GetAllNonCraftableItems(): Item[] {
    return this.localDatabase['items'].filter((item) => {
      return !item.craftable;
    })
  }

  public GetItemPrimaryColor(id: number): Color {
    return this.GetItemFromId(id).primaryColor;
  }
}
