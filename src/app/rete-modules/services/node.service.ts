import { Connection } from './../shared/connection.model';
import { TreeItem, NodeTree } from './../shared/node-tree';
import { CustomNodeData } from './../shared/custom.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export abstract class NodeService {
  private nodeTree: NodeTree;
  
  constructor() {
    this.nodeTree = new NodeTree;
  }

  public parseFromEditor(json) {
    this.nodeTree.parseFromJson(json);
  }

  public getCriticalPath(): number[] {
    return this.nodeTree.getCriticalPath();
  }

  public getAllOutputs() {
    return this.nodeTree.getOutputs();
  }

  public getNodeTreeItem(id: number) {
    return this.nodeTree.getNode(id);
  }

  public calculateCustomNodeFromOutputs(nodeOutputs: TreeItem[]): [number[], CustomNodeData] {
    let connectedNodes: number[] = [];

    // Create the custom node data to generate
    let customNodeData: CustomNodeData = new CustomNodeData([]);
    
    //TODO: Only transverse nodes that are not connected to each other

    // For all of the valid output nodes in the graph
    nodeOutputs.forEach(outputNode => {
      // Create a stack for transversal
      if (outputNode.getInputTree() != null) { 
        connectedNodes.push(outputNode.node.id);
        let connectionStack: Connection[] = [];
        
        outputNode.getInputTree().forEach(connection => {
          connectionStack.push(connection);
        })
        
        // Loop through the entire connection tree
        let outputData = { inputRate: {}, createdAmount: -1 };
        while (connectionStack.length > 0) {
          let currentConnection = connectionStack.pop();
          connectedNodes.push(currentConnection.nodeId);
          if (!currentConnection) {
            continue;
          }
          // If this is the first time seeing a node, that is the output amount
          if (outputData.createdAmount === -1) {
            outputData.createdAmount = currentConnection.amount;
          }
          
          if (currentConnection.tree.length > 0) {
            // For each value, propogate the previous rate if available and the input value amount and crafting translation cost
            currentConnection.tree.forEach((inputValue, inputIndex) => {
              let propogateOutput = outputData.inputRate[currentConnection.itemId];
              outputData.inputRate[inputValue.itemId] = inputValue.amount * currentConnection.transitionCost[inputIndex] * (propogateOutput == undefined ? 1 : propogateOutput);
              connectionStack.push(inputValue);
            });
            
            // Delete the value if it is not a resource node
            delete outputData.inputRate[currentConnection.itemId];
          }
        }
        
        // If the output has some inputs connected to it
        if (outputData.createdAmount > -1) {
          
          // Get the output item index
          let outputIndex: number = outputNode.node.data.resource['id'];
          
          // Save the output data and all its inputs
          customNodeData.outputs[outputIndex] = outputData;
          for (let inputId in outputData.inputRate) {
            if (!customNodeData.inputs.includes(parseInt(inputId))) {
              customNodeData.inputs.push(parseInt(inputId));
            }
          }
        }
      }
    });

    return [connectedNodes, customNodeData];
  }

  public abstract GetAllCustomNodes(): Promise<CustomNodeData[]>;
  public abstract ForceGetAllCustomNodes(): Promise<CustomNodeData[]>;
  public abstract GetCustomNode(uuid: string): Promise<CustomNodeData>;

  public abstract SaveCustomNode(node: CustomNodeData): Promise<string>;

  public abstract postCriticalPath_tl(uuid: string)
}
