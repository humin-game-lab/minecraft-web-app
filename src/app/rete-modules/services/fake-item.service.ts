import { Color } from './../../general/model/color.model';
import { Item } from './../../general/model/item.model';
import { Injectable } from '@angular/core';
import { ItemService } from 'src/app/rete-modules/services/item.service';

var fakeDatabase = {
  items: [
    {
      id: 1,
      name: "Stone",
      index: [0, 0],
      craftable: false,
    },
    {
      id: 2,
      name: "Grass",
      index: [1, 0],
      craftable: false,
    },
    {
      id: 3,
      name: "Dirt",
      index: [2, 0],
      craftable: false,
    },
    {
      id: 4,
      name: "Cobblestone",
      index: [3, 0],
      craftable: false,
    },
    {
      id: 5,
      name: "Planks",
      index: [4, 0],
      craftable: true,
      recipe: {
        createdAmount: 4,
        ingredients: [
          {
            id: 23,
            amount: 1
          }
        ],
      },
    },
    {
      id: 23,
      name: "Logs",
      index: [6, 1],
      craftable: false,
    },
    {
      id: 92,
      name: "Bookshelf",
      index: [11, 5],
      craftable: true,
      recipe: {
        createdAmount: 1,
        ingredients: [
          {
            id: 340,
            amount: 3
          },
          {
            id: 5,
            amount: 6
          }
        ],
      },
    },
    {
      id: 103,
      name: "Crafting Bench",
      index: [6, 6],
      craftable: true,
      recipe: {
        createdAmount: 1,
        ingredients: [
          {
            id: 5,
            amount: 4
          },
        ],
      },
    },
    {
      id: 334,
      name: "Leather",
      index: [14, 18],
      craftable: false,
    },
    {
      id: 338,
      name: "Sugar Cane",
      index: [2, 19],
      craftable: false,
    },
    {
      id: 339,
      name: "Paper",
      index: [3, 19],
      craftable: true,
      recipe: {
        createdAmount: 3,
        ingredients: [
          {
            id: 338,
            amount: 3
          },
        ],
      },
    },
    {
      id: 340,
      name: "Book",
      index: [4, 19],
      craftable: true,
      recipe: {
        createdAmount: 1,
        ingredients: [
          {
            id: 334,
            amount: 1
          },
          {
            id: 339,
            amount: 3
          }
        ],
      },
    },
  ],

  colors: [
    {
      id: 1,
      primary: '#7c7c7c'
    },
    {
      id: 2,
      primary: '#548149'
    },
    {
      id: 3,
      primary: '#a27450'
    },
    {
      id: 4,
      primary: '#464646'
    },
    {
      id: 5,
      primary: '#a48553'
    },
    {
      id: 23,
      primary: '#58462a'
    },
    {
      id: 92,
      primary: '#735c39'
    },
    {
      id: 103,
      primary: '#110e09'
    },
    {
      id: 334,
      primary: '#c65c35'
    },
    {
      id: 338,
      primary: '#aadb74'
    },
    {
      id: 339,
      primary: '#d6d6d6'
    },
    {
      id: 340,
      primary: '#654b17'
    },
  ]
};

@Injectable()
export class FakeItemService extends ItemService {
  constructor() { 
    super()
  }

  GetAllItems(): Item[] {
    return fakeDatabase["items"] as Item[];
  }

  GetItemFromId(id: number): Item {
    return fakeDatabase["items"].find((value) => {
      if (value.id == id) { return value as Item};
    })
  }

  GetAllCraftableItems(): Item[] {
    return fakeDatabase["items"].filter( value => {
      return value.craftable;
    })
  }

  GetAllNonCraftableItems(): Item[] {
    return fakeDatabase["items"].filter(value => {
      return !value.craftable;
    })
  }

  GetItemPrimaryColor(id: number): Color {
    let color = fakeDatabase['colors'].find(value => {
      return value.id == id;
    });

    return Color.FromHex(color.primary);
  }
}
