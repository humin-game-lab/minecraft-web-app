import { TreeItem } from './../shared/node-tree';
import { CustomNodeData, ShareLevel } from './../shared/custom.model';
import { NodeService } from './node.service';
import { Injectable } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';

var fakeDatabase: {nodes: CustomNodeData[]} = {
  nodes: [
    {
      uuid: "UUID",
      uniqueUserId: "user1",
      meta: {
        name: "Bookshelves",
        shareLevel: ShareLevel.Personal,
        icon: 92,
      },
      inputs: [338, 334, 23],
      outputs: {
        92: {
          inputRate: { 338: .11, 334: .33, 23: .66 },
          createdAmount: 1
        }
      },
    },
    {
      uuid: "UUID2",
      uniqueUserId: "user2",
      meta: {
        name: "Test",
        shareLevel: ShareLevel.Classroom,
        icon: 103,
      },
      inputs: [338, 334, 23],
      outputs: {
        92: {
          inputRate: { 338: .11, 334: .33, 23: .66 },
          createdAmount: 1
        },
        23: {
          inputRate: { 23: 1.0 },
          createdAmount: 1
        }
      },
    }
  ]
};

@Injectable()
export class FakeNodeService extends NodeService {

  constructor() { 
    super()
  }
  public postCriticalPath_tl(uuid: string) {
    
  }
  GetAllCustomNodes(): Promise<CustomNodeData[]> {
    return Promise.resolve(fakeDatabase['nodes']);
  }

  ForceGetAllCustomNodes(): Promise<CustomNodeData[]> {
    return Promise.resolve(fakeDatabase['nodes']);
  }

  GetCustomNode(uuid: string): Promise<CustomNodeData> {
    return Promise.resolve(fakeDatabase.nodes.find(value => {
      if (value.uuid == uuid) {
        return value;
      }
    }))
  }

  SaveCustomNode(data: CustomNodeData): Promise<string> {
    return Promise.resolve("uuid");
  }
}
