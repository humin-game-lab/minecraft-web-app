import { environment_vars } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { NodeService } from 'src/app/rete-modules/services/node.service';
import { CustomNodeData, ShareLevel } from './../shared/custom.model';
import { Injectable } from '@angular/core';
import { env } from 'process';
import { SessionService } from 'src/app/general/service/session.service';
import { isNamedTupleMember } from 'typescript';
import { promise } from 'protractor';

let getCustomNodeRoute = '/minecraft/getNodes'
let postCustomNodeRoute = '/minecraft/postNode'

let routeCriticalPath = '/minecraft/criticalpath'

const options = {
  headers: {
    Authorization: environment_vars.authentication,
  }
}

@Injectable({
  providedIn: 'root'
})
export class HttpNodeService extends NodeService {
  private localStorage: CustomNodeData[];
  constructor(private httpClient: HttpClient, private sessionService: SessionService) {
    super();

    this.localStorage = [];
  }

  public GetAllCustomNodes(): Promise<CustomNodeData[]> {
    return this.UpdateLocalStorageIfNeeded("", false);
  }

  public ForceGetAllCustomNodes(): Promise<CustomNodeData[]> {
    return this.UpdateLocalStorageIfNeeded("", true);
  }

  public GetCustomNode(uuid: string): Promise<CustomNodeData> {
    return this.UpdateLocalStorageIfNeeded(uuid, false) as Promise<CustomNodeData>;
  }

  public postCriticalPath_tl(uuid: string): Promise<CustomNodeData> {
    let body = this.getCriticalPath()
    return (this.httpClient.post(environment_vars.api_root + routeCriticalPath, body, options).toPromise() as Promise<any>).then(any => {
      this.localStorage = any['Items']
      return this.GetFromLocalStorage(uuid)
    })
  }

  public SaveCustomNode(node: CustomNodeData): Promise<string> {
    let body = this.sessionService.buildPostWithSession("nodeData", node);

    return (this.httpClient.post(environment_vars.api_root + postCustomNodeRoute, body, options).toPromise() as Promise<any>).then(any => {
      node.uuid = any['message']['uuid'];
      this.localStorage.push(node);
      return node.uuid;
    })
  }

  private GetFromLocalStorage( uuid: string ): Promise<any> {
    // If not looking for unique, return all
    if ( uuid === "" ) {
      return Promise.resolve(this.localStorage);
    }

    // Return found if have it
    let found = this.localStorage.find(value => {
      return value.uuid == uuid;
    })
    if (found) {
      return Promise.resolve(found);
    }

    let body = this.sessionService.buildPostWithSession("", null);
    return (this.httpClient.post(environment_vars.api_root + getCustomNodeRoute, body, options).toPromise() as Promise<any>).then(any => {
      this.localStorage = any['Items'];
      return this.localStorage.find(value => {
        return value.uuid = uuid;
      });
    });
  }

  private UpdateLocalStorageIfNeeded(uuid: string, forceUpdate: boolean): Promise<any> {
    let body = this.sessionService.buildPostWithSession("", null);

    if ( forceUpdate || this.localStorage.length == 0 ) {
      return (this.httpClient.post(environment_vars.api_root + getCustomNodeRoute, body, options).toPromise() as Promise<any>).then(any => {
        this.localStorage = any['Items'];

        return this.GetFromLocalStorage(uuid);
      });
    }

      return this.GetFromLocalStorage(uuid);
  }
}
