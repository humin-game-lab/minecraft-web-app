import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeBrowserComponent } from './node-browser.component';

describe('NodeBrowserComponent', () => {
  let component: NodeBrowserComponent;
  let fixture: ComponentFixture<NodeBrowserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodeBrowserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
