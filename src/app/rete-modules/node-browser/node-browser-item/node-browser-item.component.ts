import { Item } from './../../../general/model/item.model';
import { Component, Input, OnInit } from '@angular/core';

export enum NodeBrowserFilter {
  ALL,
  RESOURCE,
  CRAFTER,
  OUTPUT,
  CUSTOM,
}

class CustomItemData {
  id: number;
  item: Item;
}

export class NodeBrowserItem {
  public name: string;
  public type: string;
  public data: Item | CustomItemData;
  
  public htmlName: string;
  
  constructor(name: string, type: string, data: any) {
    this.name = name;
    this.type = type;
    this.data = data;

    this.htmlName = this.name;
  }
};

@Component({
  selector: 'node-browser-item',
  templateUrl: './node-browser-item.component.html',
  styleUrls: ['./node-browser-item.component.sass']
})
export class NodeBrowserItemComponent implements OnInit {
  @Input() nodeItem: NodeBrowserItem;
  @Input() filter: number;

  constructor() { }

  ngOnInit(): void {
  }

  onDrag(event: any) {
    let nodeType = "";
    if (this.filter == 0) {
      nodeType = this.getType();
    }
    else if (this.filter == 1) {
      nodeType = "Resource";
    }
    else if (this.filter == 2) {
      nodeType = "Crafter"
    }
    else if (this.filter == 3) {
      nodeType = "Output"
    } else if (this.filter == 4) {
      nodeType = "Custom";
    }

    event.dataTransfer.setData("nodeType", nodeType)

    event.dataTransfer.setData("nodeId", this.getId());
    event.dataTransfer.effectAllowed = "copy";
  }

  getType() {
    if (this.filter != NodeBrowserFilter.OUTPUT) {
      if (this.nodeItem.type == "item") {
        let item = <Item>this.nodeItem.data;
        return item.craftable ? "Crafter" : "Resource"
      } else {
        return "Custom"
      }
    }

    if (this.filter == NodeBrowserFilter.OUTPUT) {
      if (this.nodeItem.type == "item") {
        return "Output"
      } else {
        return "Custom"
      }
    }

  }

  getItem(): Item {
    if (this.nodeItem.type == "item") {
      return <Item>this.nodeItem.data;
    } else {
      let customData = <CustomItemData>this.nodeItem.data;
      return customData.item;
    }
  }

  getId(): any {
    return this.nodeItem.data.id;
  }
}