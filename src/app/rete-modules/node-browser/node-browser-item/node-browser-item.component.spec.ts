import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeBrowserItemComponent } from './node-browser-item.component';

describe('NodeBrowserItemComponent', () => {
  let component: NodeBrowserItemComponent;
  let fixture: ComponentFixture<NodeBrowserItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NodeBrowserItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeBrowserItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
