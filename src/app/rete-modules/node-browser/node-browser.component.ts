import { Item } from './../../general/model/item.model';
import { NodeBrowserItem, NodeBrowserFilter } from './node-browser-item/node-browser-item.component';
import { CustomNodeData } from './../shared/custom.model';
import { ItemService } from 'src/app/rete-modules/services/item.service';
import { NodeService } from 'src/app/rete-modules/services/node.service';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-node-browser',
  templateUrl: './node-browser.component.html',
  styleUrls: ['./node-browser.component.sass']
})
export class NodeBrowserComponent implements OnInit {
  public nodeList: NodeBrowserItem[];
  public customNodeList: NodeBrowserItem[];

  public filter: NodeBrowserFilter = NodeBrowserFilter.ALL;
  public searchString: string = "";

  constructor(private itemService: ItemService, private nodeService: NodeService) {
    this.nodeList = [];
    this.customNodeList = [];

    this.nodeService.GetAllCustomNodes().then(nodes => {
      this.convertNodesToNodeBrowser(nodes).forEach(value => {
        this.customNodeList.push(value);
        // Add to the global list if they should be
        if (this.filter == NodeBrowserFilter.CUSTOM || this.filter == NodeBrowserFilter.ALL) {
          this.nodeList.push(value);
        }
      });
    });
    
  }

  ngOnInit(): void {
    this.filterItems();
  }

  onAllClicked() {
    this.filter = NodeBrowserFilter.ALL;
    this.filterItems();
  }

  onResourcesClicked() {
    this.filter = NodeBrowserFilter.RESOURCE;
    this.filterItems();
  }

  onCraftableClicked() {
    this.filter = NodeBrowserFilter.CRAFTER;
    this.filterItems();
  }

  onOutputClicked() {
    this.filter = NodeBrowserFilter.OUTPUT;
    this.filterItems();
  }

  onCustomClicked() {
    this.filter = NodeBrowserFilter.CUSTOM;
    this.filterItems();
  }

  onSearchChange() {
    this.searchString = this.searchString.toLowerCase();
    this.filterItems();
  }

  private filterItems() {
    this.nodeList = [];
    if (this.filter == NodeBrowserFilter.ALL ) {
      this.nodeList = this.convertItemsToNodeBrowser(this.itemService.GetAllItems());

      // Add the custom nodes
      this.customNodeList.forEach(value => {
        this.nodeList.push(value);
      })
    }
    
    if (this.filter == NodeBrowserFilter.RESOURCE) {
      this.nodeList = this.convertItemsToNodeBrowser(this.itemService.GetAllNonCraftableItems());
    }

    if (this.filter == NodeBrowserFilter.CRAFTER) {
      this.nodeList = this.convertItemsToNodeBrowser(this.itemService.GetAllCraftableItems());
    }

    if ( this.filter == NodeBrowserFilter.OUTPUT) {
      this.nodeList = this.convertItemsToNodeBrowser(this.itemService.GetAllItems());
    }

    if (this.filter == NodeBrowserFilter.CUSTOM) {
      this.nodeList = [];
      this.customNodeList.forEach(value => {
        this.nodeList.push(value);
      })

      this.nodeService.ForceGetAllCustomNodes().then(nodes => {
        this.nodeList = [];
        this.customNodeList = [];
        // Get the Nodes
        this.convertNodesToNodeBrowser(nodes).forEach(value => {
          // Add to the custom node list
          this.customNodeList.push(value);
          if (this.filter == NodeBrowserFilter.CUSTOM || this.filter == NodeBrowserFilter.ALL) {
            this.nodeList.push(value);
          }
        })
      });
    }


    this.nodeList = this.nodeList.filter(value => {
      if (this.searchString) {
        let index = value.name.toLowerCase().indexOf(this.searchString);
        if (index >= 0) {
          value.htmlName = value.name.substring(0, index)
            + "<mark>"
            + value.name.substring(index, index + this.searchString.length)
            + "</mark>"
            + value.name.substring(index + this.searchString.length, value.name.length);
        }
        return index >= 0;
      } else {
        value.htmlName = value.name;
        return true;
      }
    })
  }

  private convertItemsToNodeBrowser(itemList: Item[]): NodeBrowserItem[] {
    let nodes: NodeBrowserItem[] = [];

    itemList.forEach((value) => {
      nodes.push(new NodeBrowserItem(value.name, "item", value));
    })

    return nodes;
  }

  private convertNodesToNodeBrowser(nodeList: CustomNodeData[]): NodeBrowserItem[] {
    let nodes: NodeBrowserItem[] = [];
    if (nodeList)
    {
      nodeList.forEach(value => {
        let data = {};
        data['id'] = value.uuid;
        data['item'] = this.itemService.GetItemFromId(value.meta.icon);
        nodes.push(new NodeBrowserItem(value.meta.name, "Custom", data));
      })
    }

    return nodes;
  }
}
