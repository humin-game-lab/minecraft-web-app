import { MinecraftNode } from './../nodes/minecraft-node.component';
import { CustomNodeMetadata } from './../shared/custom.model';
import { CustomNodeModal } from './custom-node-modal/custom-node-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ResourceControl } from './../nodes/controls/resource-control/resource-control';
import { OutputManagerService } from './../../output/services/output-manager.service';
import { CustomNode } from './../nodes/custom-node.component';
import { BaseNodeComponent } from './../nodes/base-node/base-node.component';
import { OutputNode } from './../nodes/output-node.component';
import { ItemService } from './../services/item.service';
import { CrafterNode } from './../nodes/crafter-node.component';
import { SocketController } from './../nodes/sockets';
import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { Component as ReteComponent, NodeEditor, Engine, Node } from 'rete';
import { Transform } from 'rete/types/view/area';
import ConnectionPlugin from 'rete-connection-plugin';

import { ResourceNode } from './../nodes/resource-node.component';
import { AngularRenderPlugin } from 'rete-angular-render-plugin';
import ContextMenuPlugin from 'rete-context-menu-plugin'
import { NodeService } from '../services/node.service';
import { MinecraftInventoryService } from 'src/app/inventory/services/minecraft-inventory.service';
import html2canvas from 'html2canvas';
import { UNKNOWN_ITEM } from 'src/app/general/model/item.model';
declare var BB;
@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.sass']
})
export class GridComponent implements AfterViewInit {
  public isSideBarCollapse: boolean = true;
  
  @ViewChild('nodeEditor', { static: true }) private el: ElementRef;
  private editor: NodeEditor = null;
  private engine: Engine;
  private components: { [type: string]: ReteComponent };

  public completePath: boolean = false;

  constructor(private socketController: SocketController, 
    private itemService: ItemService,
    private nodeService: NodeService,
    private minecraftInventoryService: MinecraftInventoryService,
    private outputManager: OutputManagerService,
    private modalService: NgbModal) { 
  }

  async ngAfterViewInit() {
    const container = this.el.nativeElement;
    
    this.components = {};

    this.components['Resource'] = new ResourceNode(this.socketController);
    this.components['Minecraft'] = new MinecraftNode(this.socketController, this.minecraftInventoryService);
    this.components['Crafter'] = new CrafterNode(this.socketController, this.itemService);
    this.components['Output'] = new OutputNode(this.socketController);
    this.components['Custom'] = new CustomNode(this.socketController, this.itemService, this.nodeService)

    this.editor = new NodeEditor('minecraft-web-app@0.1.0', container);
    this.editor.use(ConnectionPlugin);
    this.editor.use(AngularRenderPlugin, { component: BaseNodeComponent });
    this.editor.use(ContextMenuPlugin, {
      searchBar: false,
      delay: 100,
      nodeItems: (node: Node) => {
        if (node.meta.hasOutputFocus) {
          return {
            'Unfocus': () => {
              this.outputManager.unmarkNode(node.id);
              node.meta.hasOutputFocus = false;
            },
          }
        }
        return {
          'Focus': () => {
            this.outputManager.markNode(node.id);
            node.meta.hasOutputFocus = true;
          },
        };
      }
    });

    this.outputManager.setEditor(this.editor);

    this.engine = new Engine('minecraft-web-app@0.1.0');

    BB.editor = this.editor;
    BB.engine = this.engine;

    for (const componentKey in this.components) {
      let component = this.components[componentKey];

      this.editor.register(component);
      this.engine.register(component);
    }

    this.editor.on(['nodecreated'], (async () => {
      await this.engine.abort();
      await this.engine.process(this.editor.toJSON());
    }) as any);

    this.editor.on(['process', 'connectioncreated', 'connectionremoved'], async () => {
      this.editor.selected.clear();
      this.editor.nodes.map(n => n.update());
      
      await this.engine.abort();
      await this.engine.process(this.editor.toJSON());
      
      this.nodeService.parseFromEditor(this.editor.toJSON());
      this.updateCriticalPath();
      // this.nodeService.postCriticalPath_tl("testing");
      console.log("hello world")
      console.log(this.editor.toJSON())
      console.log(this.nodeService.getCriticalPath())

      this.outputManager.update( this.editor.nodes );

    });

    this.editor.on('noderemoved', async (node) => {
      this.editor.selected.clear();
      this.editor.nodes.map(n => n.update());
      
      await this.engine.abort();
      await this.engine.process(this.editor.toJSON());
      
      this.nodeService.parseFromEditor(this.editor.toJSON());
      this.updateCriticalPath();

      this.outputManager.unmarkNode(node.id);
      this.outputManager.update( this.editor.nodes );
    });

    this.editor.on('click', () => {
      this.editor.selected.clear();
      this.editor.nodes.map(n => n.update());
    })

    this.engine.on('error', (message) => {
      console.log(message);
    });

    // Removes the context menu on the grid
    this.editor.events['showcontextmenu'].push(({ e, node }) => {
      return Boolean(node);
    });
    
    this.editor.view.resize();
    this.editor.trigger('process');
  }

  updateCriticalPath() {
    let criticalPath = this.nodeService.getCriticalPath();
    this.completePath = false;

    this.editor.nodes.forEach(value => {
      value.meta.criticalPath = false;

      let control = <ResourceControl>value.controls.get('resource');
      if (!control) { return; }
      control.props.criticalPath = false;
    })

    criticalPath.forEach( value => {
      let node_ref = this.editor.nodes.find(n => n.id === value);
      node_ref.meta.criticalPath = true;

      let control = <ResourceControl>node_ref.controls.get('resource');
      control.props.criticalPath = true;

      this.completePath = true;
    });
  }
  
  dragOver(event) {
    event.preventDefault();

    event.dataTransfer.dropEffect = "copy";
  }

  onDrop(event) {
    event.preventDefault();

    const type = event.dataTransfer.getData("nodeType");
    const id = event.dataTransfer.getData("nodeId") as number;

    // Does not add item with invalid id
    if (id == UNKNOWN_ITEM.id) {
      console.error("Cannot create node for unknown item");
    } else {
      this.createNode(type, id, event.clientX, event.clientY, true);
    }
  }

  deleteNode() {
    this.editor.selected.list.forEach(value => {
      this.editor.removeNode(value);
    })

    this.editor.selected.list = [];
  }

  anyNodeSelected(): boolean {
    return this.editor != null && this.editor.selected.list.length > 0;
  }

  scaleToFit() {
    let editorBounds: [number, number] = [this.editor.view.area.container.clientWidth, this.editor.view.area.container.clientHeight];
    
    let firstPass = true;
    let topLeftBounds: [number, number] = [0, 0]; // Towards the negative
    let bottomRightBounds: [number, number] = [0, 0]; // Towards the positive
    
    this.editor.nodes.forEach(value => {
      if (firstPass || topLeftBounds[0] > value.position[0]) {
        topLeftBounds[0] = value.position[0];
      }
      if (firstPass || topLeftBounds[1] > value.position[1]) {
        topLeftBounds[1] = value.position[1];
      }
      
      if (firstPass || bottomRightBounds[0] < value.position[0] + 200) {
        bottomRightBounds[0] = value.position[0] + 200;
      }
      if (firstPass || bottomRightBounds[1] < value.position[1] + 250) {
        bottomRightBounds[1] = value.position[1] + 250
      };
      
      firstPass = false;
    })
    
    let nodeBounds: [number, number] = [bottomRightBounds[0] - topLeftBounds[0] + 50, bottomRightBounds[1] - topLeftBounds[1] + 50];
    let nodeCenter: [number, number] = [topLeftBounds[0] + nodeBounds[0] * .5, topLeftBounds[1] + nodeBounds[1] * .5];
    
    let transform: Transform = {k: 1, x: 0, y: 0}; 
    
    // Calculate the scale
    let sizeDifference = [editorBounds[0] / nodeBounds[0], editorBounds[1] / nodeBounds[1]];
    let widthLonger = sizeDifference[0] < sizeDifference[1];
    if (widthLonger) {
      transform.k = sizeDifference[0];
      
    }
    else {
      transform.k = sizeDifference[1];
    }
    
    let editorCenter: [number, number] = [editorBounds[0] * .5, editorBounds[1] * .5];
    let centerDifference: [number, number] = [(nodeCenter[0] - 25) * transform.k - editorCenter[0], (nodeCenter[1] - 25) * transform.k - editorCenter[1]];
    
    transform.x = -centerDifference[0];
    transform.y = -centerDifference[1];
    
    this.editor.view.area.transform = transform;
    this.editor.view.area.update();
  }

  startCollapseSequence() {
    let modelRef = this.modalService.open(CustomNodeModal);
    modelRef.result.then((nodeCreationData: CustomNodeMetadata) => {
      this.collapseToNode(nodeCreationData);
    })
      
    let modal = modelRef.componentInstance as CustomNodeModal;
    modal.setItmeIds([1, 2, 3, 4, 5, 92, 23, 103]);
  }

  collapseToNode( nodeCreationData: CustomNodeMetadata) {
    let nodeOutputs = this.nodeService.getAllOutputs();

    let result = this.nodeService.calculateCustomNodeFromOutputs(nodeOutputs);

    let connectedNodes = result[0];
    let customNodeData = result[1];

    customNodeData.meta = nodeCreationData;
    this.nodeService.SaveCustomNode(customNodeData).then(value => {
      // Removes the nodes and replaces it with a custom version of the node
    let location: [number, number] = [0, 0];
    let removedNodes: number = 0;
    connectedNodes.forEach(nodeToRemove => {
      let node = this.editor.nodes.find(node => nodeToRemove === node.id);

      location[0] += node.position[0];
      location[1] += node.position[1];
      removedNodes++;

      this.editor.removeNode(node);
    });

    location[0] /= removedNodes;
    location[1] /= removedNodes;



    this.createNode("Custom", value, location[0], location[1], false);
    });
  }

  // Reference - https://prasanthj.com/javascript/convet-div-to-image-in-angular/
  captureScreen() {
    // Solution is to change the size of the svg before I write the canvas
    this.fixSVGConnections();
    
    html2canvas(document.body, {
      // foreignObjectRendering: true,
    }).then(canvas => {
      canvas.toBlob(blob => {
        let link = document.createElement("a");
        link.download = "Screenshot.png";
        link.href = URL.createObjectURL(blob);
        link.click();
      })

      this.resetConnections();
    })


  }

  connections: HTMLCollectionOf<Element>;
  // fixSVGConnections must be run before resetConnections to populate the connections structure
  private fixSVGConnections() {
    this.connections = document.getElementsByClassName("connection");
    for (let index = 0; index < this.connections.length; ++index) {
      let connection = <HTMLElement>this.connections.item(index);

      this.zeroOutConnectionPath(connection);
    }
  }

  pathParentPadding = [5, 10];
  // TODO: Fix connections that go from low to high
  private zeroOutConnectionPath( connection: HTMLElement ) {
    let parent = connection.parentElement;
    let path = connection.children.item(0);

    let svgPath = path.getAttribute('d');
    let pathElements = svgPath.split(' ');

    let pathStart = [parseInt(pathElements[1]), parseInt(pathElements[2])];
    let pathEnd = [parseInt(pathElements[8]), parseInt(pathElements[9])];

    // Get top left point for the path with a border of 5px
    let topLeft = [Math.min(pathStart[0], pathEnd[0]) - this.pathParentPadding[0], Math.min(pathStart[1], pathEnd[1]) - this.pathParentPadding[1]];
    let bottomRight = [Math.max(pathStart[0], pathEnd[0]) + this.pathParentPadding[0], Math.max(pathStart[1], pathEnd[1]) + this.pathParentPadding[1]];
    let dimensions = [bottomRight[0] - topLeft[0], bottomRight[1] - topLeft[1]];

    // Offset the path points
    // Set the parent to the top left point
    parent.style.top = topLeft[1].toString() + "px";
    parent.style.left = topLeft[0].toString() + "px";

    // Offset the start path points 
    pathElements[1] = (pathStart[0] - topLeft[0]).toString();
    pathElements[2] = (pathStart[1] - topLeft[1]).toString();
    
    // Offset the end path points
    pathElements[8] = (pathEnd[0] - topLeft[0]).toString();
    pathElements[9] = (pathEnd[1] - topLeft[1]).toString();

    // Set the slope points to the axis aligned point at the end of the path
    pathElements[4] = pathElements[8];
    pathElements[5] = pathElements[2];
    pathElements[6] = pathElements[1];
    pathElements[7] = pathElements[9];

    // Actually calculate the width and the height
    connection.setAttribute("width", dimensions[0].toString())
    connection.setAttribute("height", dimensions[1].toString())

    let newPath = pathElements.join(' ');
    path.setAttribute('d', newPath);
  }

  private resetConnections() {
    for (let index = 0; index < this.connections.length; ++index) {
      let connection = <HTMLElement>this.connections.item(index);

      this.resetConnectionPath(connection);
    }

    this.editor.nodes.forEach(node => {
      this.editor.view.updateConnections({ node: node } );
    })
  }

  private resetConnectionPath( connection: HTMLElement) {
    let parent = connection.parentElement;
    let path = connection.children.item(0);

    let svgPath = path.getAttribute('d');
    let pathElements = svgPath.split(' ');

    let pathStart = [parseInt(pathElements[1]), parseInt(pathElements[2])];
    let pathEnd = [parseInt(pathElements[8]), parseInt(pathElements[9])];

    // Get top left point for the path with a border of 5px
    let topLeft = [Math.min(pathStart[0], pathEnd[0]) - this.pathParentPadding[0], Math.min(pathStart[1], pathEnd[1]) - this.pathParentPadding[1]];

    // Offset the path points
    // Set the parent to the top left point
    parent.style.top = "0px";
    parent.style.left = "0px";

    connection.removeAttribute('width')
    connection.removeAttribute('height');
  }
  
  private async createNode(nodeType: string, nodeId: number | string, x: number, y: number, fromMouse: boolean = false) {
    if (this.components.hasOwnProperty(nodeType)) {
      let component;
      if (nodeType == "Custom") {
        component = await this.components[nodeType].createNode({ custom: nodeId });
      } else {
        component = await this.components[nodeType].createNode({ resource: this.itemService.GetItemFromId(nodeId as number) });
      }


      let position: [number, number] = [0, 0];
      if (fromMouse) {
        let transform = this.editor.view.area.transform;
        position = [(x - transform.x - 75) / transform.k, (y - transform.y - 125) / transform.k] as [number, number];
      }
      else {
        position = [x, y];
      }

      component.position = position;
      this.editor.addNode(component);
    }
  }
}
