import { Item } from './../../../general/model/item.model';
import { CustomNodeMetadata, ShareLevel } from './../../shared/custom.model';
import { ItemService } from 'src/app/rete-modules/services/item.service';
import { NgbActiveModal, } from '@ng-bootstrap/ng-bootstrap';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

let STATIC_COLUMN_NUMBER: number = 4;

@Component({
  selector: 'custom-node-modal',
  templateUrl: './custom-node-modal.component.html',
  styleUrls: ['./custom-node-modal.component.sass']
})
export class CustomNodeModal {
  public rows: number[] = [];
  public columns: number[] = [0, 1, 2, 3];

  public nodeNameControl = new FormControl('');
  public nodeImageValue = '0,0';
  public nodeShareControl = new FormControl('Personal');
  
  public items: Item[] = [];

  constructor(public activeModal: NgbActiveModal, private itemService: ItemService) {
  }

  setItmeIds(itemIds: number[]) {
    if (itemIds.length % STATIC_COLUMN_NUMBER !== 0) {
      throw RangeError("Item ids list must be divisible by " + STATIC_COLUMN_NUMBER);
    }
    this.rows = [];

    let totalRows = Math.ceil(itemIds.length / STATIC_COLUMN_NUMBER);

    for (let row = 0; row < totalRows; ++row) {
      this.rows.push(row);
      for (let col = 0; col < STATIC_COLUMN_NUMBER; ++col) {
        let index = row * STATIC_COLUMN_NUMBER + col;

        if (index >= itemIds.length) {
          break;
        } else {
          this.items.push(this.itemService.GetItemFromId(itemIds[index]));
        }
      }
    }
  }

  getItem(row: number, col: number) {
    return this.items[row * STATIC_COLUMN_NUMBER + col];
  }

  submitCustomNode() {
    
    let customNodeMetadata = new CustomNodeMetadata();
    customNodeMetadata.name = this.nodeNameControl.value;
    if (customNodeMetadata.name.length == 0) {
      customNodeMetadata.name = "Custom Node"
    }
    
    // Get the x,y of the selected item
    let xy = this.nodeImageValue.split(',');
    let x = parseInt(xy[0]);
    let y = parseInt(xy[1]);
    
    console.log(xy);
    console.log(this.items);
    customNodeMetadata.icon = this.items[x * STATIC_COLUMN_NUMBER + y].id;
    
    switch (this.nodeShareControl.value) {
      case "Personal": customNodeMetadata.shareLevel = ShareLevel.Personal; break;
      case "Group": customNodeMetadata.shareLevel = ShareLevel.Group; break;
      case "Classroom": customNodeMetadata.shareLevel = ShareLevel.Classroom; break;
    }
    
    this.activeModal.close(customNodeMetadata);
  }

  nodeImageRadialClicked(event) {
    this.nodeImageValue = (event.originalTarget || event.srcTarget || event.target).value;
  }
}