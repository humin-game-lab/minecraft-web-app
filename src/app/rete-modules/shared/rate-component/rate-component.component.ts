import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'rate-component',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.sass']
})
export class RateComponent implements OnInit {
  @Input() readonly: boolean;
  @Input() placeholder: string | number;
  @Input() text: string;
  @Output() onChange: EventEmitter<number> = new EventEmitter();

  public perHourString: string;
  public perHour: number;

  constructor() {
  }
  
  ngOnInit(): void {
  }

  onInputChange() {
    this.perHour = parseFloat(this.perHourString);

    this.onChange.emit(this.perHour);
  }

  setRate(rate: number) {
    this.perHour = rate;

    let str = this.perHour == Infinity ? "-" : this.perHour.toString();
    if (str.indexOf('.') !== -1) {
      str = str.slice(0, str.indexOf('.') + 3);
    }

    this.perHourString = str;
  }

  public setPlaceholder( placeholder: string | number ) {
    this.placeholder = placeholder
  }
}
