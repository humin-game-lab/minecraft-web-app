import { Connection } from './connection.model';
import { Node } from 'rete';
import { isTypeNode } from 'typescript';

export class TreeItem {
  public node: Node;
  public inputs: TreeItem[];
  public outputs: TreeItem[];

  constructor(node: Node) {
    this.node = node;

    this.inputs = [];
    this.outputs = [];
  }

  getNodeId(): number {
    return this.node['id'];
  }

  getNodeName(): string {
    return this.node['name'];
  }

  getNodeData(): object {
    return this.node['data'];
  }

  getNodeResourceId(): number {
    return this.getNodeData()['resource'].id;
  }

  getNodeElementName(): string {
    return this.getNodeData()['resource'].name;
  }

  getInputTree(): Connection[] {
    return this.getNodeData()['inputTree'];
  }
}

export class NodeTree {
  public outputNodes: TreeItem[] = [];
  public criticalPath: number[] = [];

  private allConnectedNodes = [];

  parseFromJson(json: object) {
    this.outputNodes = [];
    this.allConnectedNodes = json['nodes'];

    let nodes: object[] = [];
    // Start with finding all of the output nodes
    for (const nodeIndex in json['nodes']) {
      const node = json['nodes'][nodeIndex];

      if (node['name'] === "Output") {
        nodes.push(node);
      }
    }

    nodes.forEach(value => {
      this.outputNodes.push( new TreeItem(<Node>value));
    });

    // let searchStack: TreeItem[] = [];
    // this.outputNodes.forEach( treeNode => {
    //   searchStack.push(treeNode);
    // });
    
    // while (searchStack.length > 0) {
    //   const currentNode: TreeItem = searchStack.pop();

    //   for (const inputIndex in currentNode.node.inputs) {
    //     const input = currentNode.node.inputs[inputIndex];

    //     if (input['connections'] && input['connections'].length > 0) {
    //       let newNode = this.allConnectedNodes[input['connections'][0]['node']];
    //       searchStack.push(this.addNode(newNode, currentNode));
    //     }
    //   }
    // }

    this.calculateCriticalPath();
  }

  getCriticalPath(): number[] {
    return this.criticalPath;
  }

  getOutputs() {
    return this.outputNodes;
  }

  getNode(id: number): TreeItem | null {
    // Shallow copy the outputNodes to the node stack to search
    let nodeStack = [...this.outputNodes];

    while (nodeStack.length > 0) {
      let nodeItem = nodeStack.pop();

      if (nodeItem.node.id == id) {
        return nodeItem;
      }

      nodeItem.inputs.forEach(value => {
        nodeStack.push(value);
      })
    }

    return null;
  }

  private calculateCriticalPath() {
    this.criticalPath = [];

    let sortedOutputNodes = this.outputNodes.sort((a, b) => {
      // Sort the focused node to the front
      if (a.getNodeData()['hasOutputFocus']) {
        return -1;
      } else {
        return a.getNodeData()['perHour'] - b.getNodeData()['perHour'];
      }
    });

    // Starting at the output nodes
    sortedOutputNodes.forEach(value => {

      // Check to see if the node is currently connected to a critical path
      if (!this.isConnectedToCriticalPath(value)) {
        this.criticalPath.push(value.node.id);
        
        // Search through the tree
        let smallestConnectionStack: Connection[] = [];
        value.getInputTree().forEach((value) => {
          smallestConnectionStack.push(value);
        })

        while (smallestConnectionStack.length > 0) {
          let currentSmallestConnection = smallestConnectionStack.pop();
          if (!currentSmallestConnection) { continue; }
          this.criticalPath.push(currentSmallestConnection.nodeId);

          let smallestPerHour = Infinity;
          let smallestConnection: Connection = null;
          let duplicateConnections: Connection[] = [];

          // Find the smallest per hour connection with duplicates
          currentSmallestConnection.tree.forEach((inputConnection, index) => {
            let inputConnectionPerHour = currentSmallestConnection.transitionCost[index] * inputConnection.amount * inputConnection.perHour;
            if (smallestPerHour === Infinity) {
              smallestPerHour = inputConnectionPerHour;
              smallestConnection = inputConnection;
            } else if (smallestPerHour === inputConnectionPerHour) {
              duplicateConnections.push(inputConnection);
            } else if (smallestPerHour > inputConnectionPerHour) {
              smallestPerHour = inputConnectionPerHour;
              smallestConnection = inputConnection;
              duplicateConnections = [];
            }
          });

          // If no connection was found this is the end of the path
          if (smallestConnection === null) {
            break;
          }

          // Add the found connections to the stack to seach
          duplicateConnections.push(smallestConnection);
          duplicateConnections.forEach(value => {
            smallestConnectionStack.push(value);
          });
        }
      }
    });
  }

  private isConnectedToCriticalPath(node: TreeItem): boolean {
    let nodeConnections = node.getInputTree();

    if( nodeConnections) {
      let allConnectionNodeIds: number[] = [];
      nodeConnections.forEach(connection => {
        let connections = connection.getAllConnectionNodeIds();

        connections.forEach(value => {
          allConnectionNodeIds.push(value);
        })
        
      });

      // Used to get the intersection of [criticalPath] x [allConnecitonNodeIds]
      return this.criticalPath.filter(value => allConnectionNodeIds.includes(value)).length > 0;
    }
    else {
      // If there is not connection, there will not be a critical path so skip
      return true;
    }
  }

  private addNode(node: any, parent: TreeItem): TreeItem {
    let child = new TreeItem(<Node>node);
    child.outputs.push(parent);

    parent.inputs.push(child);

    return child;
  }
}