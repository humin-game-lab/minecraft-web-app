import { Item } from './../../general/model/item.model';

export class Connection {
  public nodeId: number;
  public itemId: number;
  public amount: number;
  public perHour: number;
  public transitionCost: number[];
  public tree: Connection[];

  constructor(nodeId: number, itemId: number, amount: number, perHour: number) {
    this.nodeId = nodeId;
    this.itemId = itemId;
    this.amount = amount;
    this.perHour = perHour;
    this.transitionCost = [];
    this.tree = [];
  }

  public getAllConnectionNodeIds(): number[] {
    let nodeIds: number[] = [];
    let treeStack: Connection[] = [];
    treeStack.push(this);

    while (treeStack.length > 0) {
      let currentConnection = treeStack.pop();
      if (currentConnection) {
        nodeIds.push(currentConnection.nodeId);
  
        currentConnection.tree.forEach(value => {
          if (!nodeIds.includes(value.nodeId)) {
            treeStack.push(value);
          }
        })
      }

    }

    return nodeIds;
  }
}