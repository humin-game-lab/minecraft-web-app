import { Output, Socket } from "rete"

export class CustomOutput extends Output {
  public data: {
    [key: string]: unknown;
  };

  constructor(key: string, title: string, socket: Socket, multiConns?: boolean) {
    super(key, title, socket, multiConns);

    this.data = {};
  }
}