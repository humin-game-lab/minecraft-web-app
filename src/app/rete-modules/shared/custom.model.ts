export enum ShareLevel {
  Personal,
  Group,
  Classroom
}

export class CustomNodeMetadata {
  public name: string;
  public shareLevel: ShareLevel;
  public icon: number;
}


export class CustomNodeData {
  public uuid: string;
  public uniqueUserId: string;
  public meta: CustomNodeMetadata;
  public inputs!: number[];
  public outputs!: { [outputIndex: number]: { inputRate: { [input: number]: number }, createdAmount: number } };

  constructor(inputs: number[], name: string = "NOT-CREATED", shareLevel: ShareLevel = ShareLevel.Personal) {
    this.meta = {name: name, shareLevel: shareLevel, icon: 1};
    this.inputs = inputs;
    this.outputs = {};
  }
}