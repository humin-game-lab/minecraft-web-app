import { Item } from './../../general/model/item.model';
import { Component, Input, Node } from 'rete';
import { SocketController } from './sockets';
import { ResourceControl } from './controls/resource-control/resource-control';

export class OutputNode extends Component {

  constructor(private socketController: SocketController) {
    super('Output');
  }

  builder(node) {
    var control = new ResourceControl(this.editor, 'resource');
    control.props.readonly = true;
    node.addControl(control);

    node.meta.type = 'output'
    node.data.name = 'Output'

    this.updateNodeStructure(node);

    return node;
  }

  worker(node, inputs, outputs) {
    var perHourBottleneck = Infinity;
    
    if (inputs && inputs['resource'] && inputs['resource'][0]) {
      perHourBottleneck = inputs['resource'][0].perHour * inputs['resource'][0].amount;
      
      // Store the completed input tree
      node.data.inputTree = [];
      node.data.inputTree.push(inputs['resource'][0]);
    } else {
      node.data.inputTree = null;
    }
    
    node.data.perHour = perHourBottleneck;
    node.data.amount = 1;
    
    var node_ref = this.editor.nodes.find(n => n.id === node.id);
    let control = <ResourceControl>node_ref.controls.get('resource');
    control.setRate(perHourBottleneck);
  }

  private updateNodeStructure(node: Node ) {
    // node.getConnections().forEach(element => {
    //   this.editor.removeConnection(element);
    // });

    // node.outputs.clear();
    var item = <Item>node.data.resource;

    var input = new Input("resource", item.name, this.socketController.GetSocketForId(item.id), false);
    node.addInput(input);
    
    this.editor.trigger('nodeselected');
  }
}