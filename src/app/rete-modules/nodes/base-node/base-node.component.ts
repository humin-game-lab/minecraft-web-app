import { Item } from './../../../general/model/item.model';
import { ChangeDetectorRef, Component } from '@angular/core';
import { NodeComponent, NodeService } from 'rete-angular-render-plugin';

@Component({
  templateUrl: './base-node.component.html',
  styleUrls: ['./base-node.component.sass'],
  providers: [NodeService],
})
  
export class BaseNodeComponent extends NodeComponent {

  constructor(protected service: NodeService, protected cdr: ChangeDetectorRef) {
    super(service, cdr);
  }

  setItem( item: Item ) {
    this.node.data.resource = item;
  }

  getType() {
    return this.node.meta.type;
  }

  onCriticalPathClass() {
    return this.node.meta.criticalPath ? "critical-path" : "";
  }

  isCustomClass() {
    return this.node.data.customization == null ? "" : "custom";
  }

  isCustom() {
    return this.node.data.customization != null;
  }

  isOutputFocused() {
    return this.node.meta.hasOutputFocus;
  }
}
