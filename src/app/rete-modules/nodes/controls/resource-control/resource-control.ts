import { Item } from './../../../../general/model/item.model';
import { RateComponent } from './../../../shared/rate-component/rate-component.component';

import {
  Component, Type, ViewChild
} from '@angular/core';
import { Control, NodeEditor } from 'rete';
import { AngularControl } from 'rete-angular-render-plugin';

@Component({
  templateUrl: "./resource-control.html",
  styleUrls: ["./resource-control.sass",]
})
export class ResourceComponent {
  public criticalPath: boolean;

  public mounted!: Function;
  public delayedRateSet!: Function;
  public changeRate!: Function;
  
  public selectedItem!: Item;
  public readonly: boolean;
  public disableRate: boolean;

  @ViewChild(RateComponent) rateComponent: RateComponent;
  
  ngOnInit() {
    this.mounted();
  }

  ngAfterViewInit() {
    this.delayedRateSet();
  }
  
  onRateChange(value) {
    this.changeRate(value);
  }

  setRate(value) {
    this.rateComponent.setRate(value);
  }
}

export class ResourceControl extends Control implements AngularControl {
  component: Type<ResourceComponent>
  props: { [key: string]: unknown }

  private unitializedSetRate = false;
  private unitializedSetRateValue = 0;
  
  constructor(public emitter: NodeEditor, public key, public disableRate: boolean = false) {
    super(key);
  
    this.component = ResourceComponent;
    this.props = {
      selectedItem: Item,
      readonly: false,
      disableRate: this.disableRate,
      changeRate: v => this.onChangeRate(v),
      mounted: () => {
        this.setInitialItem();
      },
      delayedRateSet: () => {
        this.delayedRateSet();
      },
      rateComponent: RateComponent,
      criticalPath: false,
    }
  }

  onChangeRate(rate: number) {
    this.setRate( rate);
    
    this.emitter.trigger('process');
  }

  getRate(): number {
    let value: number = this.getData('perHour') as number;
    if (value == undefined) {
      value = NaN;
    }

    return value;
  }

  // Used when you want to just set the visual
  setRate(rate: number) {
    if (!this.props.disableRate) {

      // Handles setting rate on uninitialized rate component
      //  Saves the value and sets in initialized
      if (typeof this.props.rateComponent === 'function') {
        this.unitializedSetRate = true;
        this.unitializedSetRateValue = rate;
        return;
      }
      (<RateComponent>this.props.rateComponent).setRate(rate);
    }

    this.putData('perHour', rate);
  }

  setPlaceholder(placeholder: string | number) {
    if (!this.props.disableRate) {
      (<RateComponent>this.props.rateComponent).setPlaceholder(placeholder);
    }
  }

  private delayedRateSet() {
    if (this.unitializedSetRate) {
      this.setRate(this.unitializedSetRateValue);
      this.unitializedSetRate = false;
    }
  }

  private setInitialItem() {
    let item = this.getData(this.key) as Item;
    this.props.selectedItem = item;
    
    this.putData(this.key, item);
    this.putData('amount', (item && item.craftable) ? item.recipe.createdAmount : 1)
    this.putData('perHour', this.props.perHour);

    this.getNode().meta['changed'] = true;
  }
}