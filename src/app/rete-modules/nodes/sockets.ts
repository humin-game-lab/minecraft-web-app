import { ItemService } from 'src/app/rete-modules/services/item.service';
import { Socket } from "rete";
import { Injectable } from '@angular/core';

@Injectable()
export class SocketController {
  private sockets: Map<number, Socket>;

  constructor(private itemService: ItemService) {
    this.sockets = new Map();
  }

  GetSocketForId(id: number): Socket {
    if (this.sockets.has(id)) {
      return this.sockets.get(id);
    }
    else {
      this.sockets.set(id, new Socket(this.itemService.GetItemFromId(id).name));
      return this.sockets.get(id);
    }
  }
}