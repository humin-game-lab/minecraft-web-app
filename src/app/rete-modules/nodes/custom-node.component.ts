import { CustomOutput } from './../shared/custom-output';
import { NodeData, WorkerInputs, WorkerOutputs } from 'rete/types/core/data';
import { CustomNodeData, ShareLevel } from './../shared/custom.model';
import { Connection } from './../shared/connection.model';
import { SocketController } from './sockets';
import { ItemService } from '../services/item.service';
import { NodeService } from '../services/node.service';
import { Component, Input, Node } from 'rete';
import { ResourceControl } from './controls/resource-control/resource-control';
import { v4 as uuidv4 } from 'uuid';


let defaultCustomData: CustomNodeData = {
  uuid: "THIS IS SET FROM THE SERVER",
  uniqueUserId: "SERVER",
  meta: {
    name: "Bookshelves",
    shareLevel: ShareLevel.Personal,
    icon: 92,
  },
  inputs: [338, 334, 23],
  outputs: {
    92: {
      inputRate: { 338: .11, 334: .33, 23: .66 },
      createdAmount: 1
    }
  },
};


export class CustomNode extends Component {

  constructor(private socketController: SocketController, private itemService: ItemService, private nodeService: NodeService) {
    super('Custom');
  }

  builder(node: Node): any {
    node.data['customization'] = defaultCustomData;
    this.updateNodeStructure(node);

    node.meta.type = 'custom'
    
    return node;
  }

  worker(node: NodeData, inputs: WorkerInputs, outputs: WorkerOutputs) {
    // Define the datagrams to output
    let datagramOutputs: { [index: number]: Connection } = {};
    node.data.inputTree = [];
    let customData = <CustomNodeData>node.data.customization;
    let customIcon = this.itemService.GetItemFromId(customData.meta.icon);

    
    var node_ref = this.editor.nodes.find(value => { return value.id == node.id });
    var control = node_ref.controls.get("resource") as ResourceControl;
    if( !control ) { return; }   // If there is no control, skip for right now
    control.props.selectedItem = customIcon;
    
    // For each output calculate the critical path based on their inputs
    for (let outputId in customData.outputs) {
      let missingInput = false;
      let perHour = Infinity;
      let output = customData.outputs[outputId];

      // Create the initial datagram for the output
      datagramOutputs[outputId] = new Connection(node.id, parseInt(outputId), output.createdAmount, perHour);

      // For each of the inputs, calculate the per hour rate
      for (let inputId in output.inputRate) {
        let input = <Connection>inputs[inputId][0];
        
        if (input && !missingInput) {
          let possiblePerHour = output.inputRate[inputId] * input.perHour * input.amount;
          if (possiblePerHour < perHour) {
            perHour = possiblePerHour;
          }
        } else {
          perHour = Infinity;
          missingInput = true;
        }

        // Add the input to the datagram tree
        if (input) {
          datagramOutputs[outputId].transitionCost.push(output.inputRate[inputId]);
          datagramOutputs[outputId].tree.push(input);

          // Look to see if the connection is already added as an input
          let connectionPresent = (<Connection[]>node.data.inputTree).find(value => {
            return value.itemId == input.itemId;
          });
        
          // Update the inputTree
          if (!connectionPresent) {
            (<any[]>node.data.inputTree).push(input);
          }
        }
      }

      // Update the datagram with the smallest per hour rate for this output
      datagramOutputs[outputId].perHour = perHour;
      let outputControl: CustomOutput = <CustomOutput>node_ref.outputs.get(outputId);
      outputControl.data.rate = perHour;
    }

    // Find the smallest critical path time for the global critical path
    let smallestPerHour: {index: any, perHour: number} = {index: 0, perHour: Infinity};
    for (let outputIndex in datagramOutputs) {
      let datagram = datagramOutputs[outputIndex];
      if (smallestPerHour.perHour > datagram.perHour) {
        smallestPerHour.index = outputIndex;
        smallestPerHour.perHour = datagram.perHour;
      }

      // Add the datagrams to the output indexes
      outputs[outputIndex] = datagramOutputs[outputIndex];
    }
  }

  private updateNodeStructure(node: Node) {
    this.nodeService.GetCustomNode(node.data['custom'] as string).then(customization => {
      if (!customization) {
        throw new Error("Failed to get Custom Node Data for ID: " + node.data['custom']);
      }
      node.data.name = customization.meta.name;
      node.data['customization'] = customization;

      // TL
      console.log(this.nodeService.getCriticalPath())
  
      // Add a ResourceControl to the node, temporaraly set icon to crafting table
      var control = new ResourceControl(this.editor, 'resource', true);
      control.props.readonly = true;
      node.addControl(control);
      
      // Add the starting data to the control
      control.putData('resource', this.itemService.GetItemFromId(customization.meta.icon));
      
      var custom: CustomNodeData = node.data.customization as any;
      custom.inputs.forEach(id => {
        var item = this.itemService.GetItemFromId(id);
        var input = new Input("" + item.id, item.name, this.socketController.GetSocketForId(item.id), false);
        node.addInput(input);
      });

      for (let outputIndex in custom.outputs) {
        let item = this.itemService.GetItemFromId(parseInt(outputIndex));
        var output = new CustomOutput("" + item.id, item.name, this.socketController.GetSocketForId(item.id), false);
        output.data.rate = Infinity;
        node.addOutput(output);
      }
    })
  }
}