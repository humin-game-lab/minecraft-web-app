import { Item } from './../../general/model/item.model';
import { Connection } from './../shared/connection.model';
import { Component, Output, Node } from 'rete';
import { SocketController } from './sockets';
import { ResourceControl } from './controls/resource-control/resource-control';

export class ResourceNode extends Component {

  constructor(private socketController: SocketController) {
    super('Resource');

  }

  builder(node) {
    var control = new ResourceControl(this.editor, 'resource');
    node.addControl(control);

    node.meta.type = 'resource'
    node.data.name = 'Resource';

    this.updateNodeStructure(node);

    return node;
  }

  worker(node, inputs, outputs) {
    let datagram = new Connection(node.id, node.data.resource.id, node.data.amount, node.data.perHour);
    outputs['resource'] = datagram;
  }

  private updateNodeStructure(node: Node ) {
    var item = <Item>node.data.resource;

    var output = new Output("resource", item.name, this.socketController.GetSocketForId(item.id), false);
    node.addOutput(output);
    
    this.editor.trigger('nodeselected');
  }
}