import { Item } from './../../general/model/item.model';
import { Connection } from './../shared/connection.model';
import { Component, Output, Node } from 'rete';
import { SocketController } from './sockets';
import { ResourceControl } from './controls/resource-control/resource-control';
import { MinecraftInventoryService } from 'src/app/inventory/services/minecraft-inventory.service';

export class MinecraftNode extends Component {

  constructor(private socketController: SocketController, private minecraftInventoryService: MinecraftInventoryService) {
    super('Minecraft');
  }

  builder(node) {
    var control = new ResourceControl(this.editor, 'resource');
    control.props.readonly = true;
    control.props.selectedItem = node.data.resource;
    node.addControl(control);

    node.meta.type = 'minecraft'
    node.data.name = 'Minecraft';
    
    this.updateNodeStructure(node);
    this.minecraftInventoryService.AssignInventoryUpdateCallback(this, node.id, node.data.resource.id, this.updateAverage);

    return node;
  }

  worker(node, input, outputs) {
    let datagram = new Connection(node.id, node.data.resource.id, node.data.amount, node.data.perHour);
    outputs['resource'] = datagram;
  }

  public updateAverage(nodeNumber: number, avg: number) {
    let node_ref: Node = this.editor.nodes.find(n => n.id === nodeNumber);

    if (node_ref) {
      let resourceContol = node_ref.controls.get('resource') as ResourceControl;
      resourceContol.onChangeRate(avg);
    }
  }

  private updateNodeStructure(node: Node ) {
    var item = <Item>node.data.resource;

    var output = new Output("resource", item.name, this.socketController.GetSocketForId(item.id), false);
    node.addOutput(output);

    this.minecraftInventoryService.GetInventoryItemAverage(node.data.resource['id']).then(value => {
      let control = node.controls.get('resource') as ResourceControl;
      control.onChangeRate(value);
    });

    this.editor.trigger('nodeselected');
  }
}