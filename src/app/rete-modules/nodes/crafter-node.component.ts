import { Item } from './../../general/model/item.model';
import { Connection } from './../shared/connection.model';
import { SocketController } from './sockets';
import { ItemService } from '../services/item.service';

import { Component, Output, Input, Node } from 'rete';
import { NodeData, WorkerInputs, WorkerOutputs } from 'rete/types/core/data'
import { ResourceControl } from './controls/resource-control/resource-control';

import { Recipe } from '../../general/model/recipe.model'

export class CrafterNode extends Component {

  constructor(private socketController: SocketController, private itemService: ItemService) {
    super('Crafter');
  }

  builder(node) {
    var control = new ResourceControl(this.editor, 'resource');
    control.props.readonly = true;
    node.addControl(control);

    node.meta.type = 'crafter';
    node.data.name = 'Crafter';

    this.updateNodeStructure(node);

    return node;
  }

  worker(node: NodeData, inputs: WorkerInputs, outputs: WorkerOutputs) {
    let perHourBottleneck = Infinity;
    let craftedItem = <Item>node.data.resource;
    let recipe: Recipe = craftedItem.recipe;
    
    // Perpare datagram for calculations
    node.data.inputTree = [];
    let datagram = new Connection(node.id, craftedItem.id, recipe.createdAmount, perHourBottleneck);
    recipe.ingredients.forEach(ingredient => {
      let input = inputs[ingredient.id];
      
      if (input && input[0]) {
        let inputData = <Connection>input[0];
        let itemBottleneck = inputData.amount / ingredient.amount;
        itemBottleneck *= inputData.perHour;
        
        if (itemBottleneck < perHourBottleneck) {
          perHourBottleneck = itemBottleneck;
        }
        
        // Add the input to the tree
        datagram.transitionCost.push(1 / ingredient.amount);
        datagram.tree.push(inputData);
        
        (<any[]>node.data.inputTree).push(inputData);
      }
    })
    
    // Find the outputs per hour bottleneck
    datagram.perHour = perHourBottleneck;
    node.data['perHour'] = perHourBottleneck;
    
    // Update the control with the rate
    var node_ref = this.editor.nodes.find(n => n.id === node.id);
    let control = <ResourceControl>node_ref.controls.get('resource');
    control.setRate(perHourBottleneck * recipe.createdAmount );
    
    outputs['resource'] = datagram;
  }

  private updateNodeStructure(node: Node) {
    var item = <Item>node.data.resource;
    var output = new Output("resource", item.name  + " x " + item.recipe.createdAmount, this.socketController.GetSocketForId(item.id), false);
    node.addOutput(output);

    item.recipe.ingredients.forEach(ingredient => {
      var item = this.itemService.GetItemFromId(ingredient.id);
      var input = new Input("" + item.id, item.name + " x " + ingredient.amount , this.socketController.GetSocketForId(item.id), false);
      node.addInput(input);
    });

    this.editor.trigger('nodeselected');
  }
}