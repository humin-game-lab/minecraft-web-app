import { InventoryModule } from './../inventory/inventory.module';
import { CustomNodeModal } from './grid/custom-node-modal/custom-node-modal.component';
import { GeneralModule } from './../general/general.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FakeNodeService } from './services/fake-node.service';
import { HttpNodeService} from './services/http-node.service'
import { ItemService } from './services/item.service';
import { ResourceComponent } from './nodes/controls/resource-control/resource-control';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridComponent } from './grid/grid.component';
import { ReteModule } from 'rete-angular-render-plugin';
import { BaseNodeComponent } from './nodes/base-node/base-node.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NodeBrowserComponent } from './node-browser/node-browser.component';
import { NodeService } from './services/node.service';
import { RateComponent } from './shared/rate-component/rate-component.component';
import { NodeBrowserItemComponent } from './node-browser/node-browser-item/node-browser-item.component';

@NgModule({
  declarations: [
    GridComponent,
    BaseNodeComponent,
    ResourceComponent,
    NodeBrowserComponent,
    RateComponent,
    CustomNodeModal,
    NodeBrowserItemComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ReteModule,
    NgbModule,
    GeneralModule,
    InventoryModule,
  ],
  providers: [{ provide: ItemService }, {provide: NodeService, useClass: HttpNodeService } ],
  exports: [GridComponent, NodeBrowserComponent],
  entryComponents: [ResourceComponent,
    BaseNodeComponent
  ]
})
export class CustomReteModule { }
