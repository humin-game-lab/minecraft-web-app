# Minecraft Factory Planner TDD

Jake Rowland

## Changelog

* 1.0 Initial Version

## Setup

### Requirements

* Node.js (using v14.9)
* Node Package Manager (NPM) (using v6.14.8)

### Instructions

1. Clone repo 
2. `npm install`
3. `npm start` runs local webserver for development
4. `npm build` runs build process to generate deployable project in /dist

## Overview

The Minecraft factory planner is a web based application designed to supplement the teaching of some STEM+C learning standards. Designed inside the Minecraft universe, the application's focus is to have a more teachable platform that connects to in game assignments to facilitate teaching. The application is broken up to 4 major components. The main component is the **editor** component in the middle of the screen. The **editor** component displays *nodes*. These nodes represent steps in planning out a factory. Nodes can connect to each other with *inputs* being on the left side of the node, and *outputs* being on the right side of the node. Below the **editor** component is the **browser** component on the left. This component displays the possible items, as well as different uses they can be: resources, crafted, output. Finally there can be custom nodes that are also displayed here. The user can drag and drop these nodes into the **editor** component. To the right of the **browser** component, there is the **Minecraft** component. Here, the inventory of the user is either, constantly updated from a Minecraft instance, or saved off and used from a previous assignment. The **Minecraft** component acts similar to the **browser** component where nodes can be dragged to the **editor** component and used. The last component is the **output** component. Nodes in the **editor** component can be selected as *focused*. These *focused* nodes appear in the **focused** subtab of the **output** component. The *focused* nodes in the **output** component display data input and output rates of the *focused* node. If the *focused* node has any inputs connected, these can be viewed by selecting the dropdown button of the *focused* node to step down the input tree. The **graph** component is the second subtab of the **output** component. **Only *focused* output nodes show up in the graph component**. The **graph** component is designed to allow students to gather statistics about specific nodes based on their inputs. The first graph, the line graph, displays resource production over time. The bar chart displays *input* resource utilization between [0-1]->[0%-100%] usage efficiency. Finally, the pie graph displays final productions stats. Of all of the resources generated, including surpluses, what percentage of all the resources, was that specific resource responsible for. 

## Future Developers

### Updating Items

Items are fundamental to the application, they define what can be used, what can be crafted, and ratios. The item *database* is defined in `/assets/resources/items.json` and follow a specific structure

```json
{
      "id": 5,				 // *Unique* id (minecraft id)
      "name": "Planks",		 // User facing name
      "index": [4, 0],		 // Sprite coordinates in /assets/images/inventory_sprites.png
      "craftable": true,	 // Is the item craftable
      "recipe": {			 // *Required* on craftable objects
        "createdAmount": 4,	 // How many are created from each crafting
        "ingredients": [	 // List of ingredients and amounts required
          {
            "id": 23,		 // Unique id referencing another item
            "amount": 1		 // Amount needed in the crafting process
          }
        ]
      },
      "primaryColor": "#7851a9",	// Primary color: graph color
      "secondaryColor": "#FF7034"	// Secondary color: not used
 }
```

Any item can be added in this manor including custom, non standard Minecraft items. However, if the item does not have a sprite associated with it, an update `/assets/images/inventory_sprites.png` will be needed.

#### Thoughts on Wool (Multi-item treated as singular item)

In Minecraft, there are items that simply need a group type such as beds. They require 3 *Wool* and 3 *Planks*, the color of the *Wool*, and the type of *Planks* are not relevant to the recipe. However, in the Minecraft Factory Planner, these types of recipes cannot exist currently.

Proposed solution would be to keep the traditional `id` and add `subtype id` that differentiates items of the same type. Any sockets, recipes, group items would use the `id` to determine the items accepted. The `subtype id` would simply be used to differentiate **Red** wool vs **Blue** wool. For sockets, and recipes, this distinction is not necessary. The side effects of this change would relate to `ItemService.GetItemFromId` & `ItemService.GetItemPrimaryColor` that accept `id` as a parameter. They will need to accept a secondary `subtype id` to get specific items in a group. Items without groups, *Stone* would simply have the `subtype id = 1`. The addition of groups would not effect the sprite calculation because items in subgroups will still have unique item definitions in *item.json* 

### Graphs

Currently in ngx-charts, there is a bug related to legends below the graph. The size of the legend is not included in the total size calculation so when using a relative sizing, the graph continually expands because it calculates its size, legend gets added, parent expands to fit it, recalculates new size based on parents, legend gets added...

There is a [current PR](https://github.com/swimlane/ngx-charts/pull/1520) that fixes it. Waiting to have is merged into the main branch. The current fix is to force a max height and then have a padding on the bottom to account for the extra space of the legend. This does not work when the legend grows larger than the padding, resulting in a cutoff legend.

### Screenshots

I used html2canvas to capture the full application on a png that can be submitted to Canvas, there was some difficulties getting it to draw all of the require elements:

* Paths between nodes
* Rates
* Visible Nodes
* **Output** component

The main issue was either the rates, or the paths not being accurately drawn to the image. html2canvas has an option `foreignObjectRendering`, and when true, passed all the requisite styles to the png. However, it failed to push any of the user entered rates so it can not be used. However, the svg paths would not be using the default value false for `foreignObjectRendering`. The issue revolved around the svg parent element not fully encapsulating the actual path. Because the **editor** graph is positioned absolutely, all of the svg had offsets of $0,0$ and start end points based on their connection points. 

#### html2Canvas Hack

To get the SVG paths showing, before the document is passed to html2canvas, preprocessing on the SVG is done.

1. Screenshot requested
2. For all svg connections
   1. Transform parent div to top left corner of svg path bounding box
   2. Recalculate path with new offset (-transform)
   3. Set SVG `width` and `height` attributes
3. Take screenshot
4. For all svg connections
   1. Revert previous parent div transformation
   2. Recalculate path to previous location
   3. Remove SVG width and heigth attributes

Reverting the paths back to normal after the transformation is to preserve the format *rete.js* expects when transforming nodes.

## Table Of Contents

### [Editor](./docs/Editor.md)

### [Block Browser](./docs/Block Browser.md)

### [Minecraft Browser](./docs/Minecraft Browser.md)

### [Output Browser](./docs/Output Browser.md)





